package BEAM2;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;

class Multithreading extends Thread{  
	private 
	String[] args;
	ArrayList<Motif> inputSequences;
	String baseName;
	SummarizedData sumData;
	String backgroundInput;
	String method;

	public Multithreading(String args[], ArrayList<Motif> inputSequences, String baseName, SummarizedData sumData, String backgroundInput,
			String method){
		this.args=args;
		this.inputSequences=inputSequences;
		this.baseName=baseName;
		this.sumData=sumData;
		this.backgroundInput=backgroundInput;
		this.method=method;

	}
	public void run(){  
		IO.INFO(this.method + " thread is in running state.");  
		try {
			MultiRun.multirun(this.args, this.inputSequences, this.baseName, this.sumData, this.backgroundInput, this.method);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}   
	public static void main(String args[]) throws IOException, InterruptedException{  

		long startTime = System.currentTimeMillis();
		
		ArrayList<Motif> inputSequences=new ArrayList<Motif>();
		ArrayList<Motif> inputSequences_nuc=new ArrayList<Motif>();

		String libName = "BEAM_lib";
		String baseName = "lastrun";
		String backgroundInput = ""; //DEFAULT rfam?
		boolean USERBG=false;
		boolean nucleoFlag=false;
		SummarizedData sumData = new SummarizedData();
		//genero inputSequences

		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		if(clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			baseName = tmp[tmp.length-1].split("\\.")[0]; //
			IO.readInput(clp.getValue("f"), inputSequences);
			IO.readInput(clp.getValue("f"), inputSequences_nuc);

		}
		if(clp.containsKey("g")){
			backgroundInput = clp.getValue("g");
			USERBG = true;
		}
		if(clp.containsKey("N")){
			nucleoFlag=true;
		}
		String outFolder="results/" + baseName;
		
		
		
		final File dir = new File(outFolder);
		if(dir.exists()){
			boolean parent = false;
			IO.deleteFolder(outFolder, parent);
		}

		String outBench=outFolder+"/benchmark";
		String outBest = outBench + "/motifs";
		String outLogo=outFolder+"/webLogoOut";
		String outBestLogo= outLogo + "/motifs";
		dir.mkdirs();

		//Copy the input in the output folder
		File source = new File(clp.getValue("f"));
		File dest = new File(outFolder+"/"+baseName + ".fa");
		IO.copyUsingFileChannels(source,dest);

		new File(outBench).mkdirs();
		new File(outBest).mkdirs();
		new File(outLogo).mkdirs();
		new File(outBestLogo).mkdirs();

		if(!USERBG){
			//se non e' presente un background inserito dall'utente, prova comunque a stimarlo
			//da Rfam, con i criteri di LSbin
			String bin=sumData.computeBin();

			//backgroundInput = IO.defaultBgCreation(bin, inputSequences.size(), outFolder);
			//in assunzione gaussiana della distribuzione degli score va bene anche se uso sempre 250
			backgroundInput = IO.defaultBgCreation(bin, 250, outFolder);			

		}

		String[] bgTmp = backgroundInput.split("/");
		sumData.setBgFile(bgTmp[bgTmp.length - 1].split("\\.")[0]);
		//NOTA: sono anche gli unici fissi (gli altri cambiano a ogni run)

		//Parallelize!

		try{
			//in this way it should be parallelizable

			Multithreading t_structure= new Multithreading(args, inputSequences, baseName, sumData, backgroundInput, "structure");

			t_structure.start();
			if (nucleoFlag){
				Multithreading t_sequence=new Multithreading(args, inputSequences_nuc, baseName, sumData, backgroundInput, "sequence");
				t_sequence.start();
				t_sequence.join();
			}
			t_structure.join();

		}catch(Exception e){
			IO.ERR(e.toString());;
		}
		
		//Post analysis: concurrency, mutual info

		MutualInfo.postAnalysis(outBest, clp.getValue("f"));

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		IO.INFO("plotting distances...");
		IO.INFO("current dir:" + new File(".").getCanonicalPath());
		
		try{
			//copy the file outside otherwise it cannot be executed
			String fullPath = IO.ExportResource("/post_analysis.py");
    	    
			final File distanceDir = new File(outFolder+"/distance");
			distanceDir.setWritable(true, true);
			distanceDir.mkdir();
			ProcessBuilder pb = new ProcessBuilder();
			//pb.command("which", "python");

			pb.command("python","post_analysis.py", outFolder, "&&", "rm", "post_analysis.py");
			Process p = pb.redirectError(Redirect.INHERIT).redirectOutput(Redirect.INHERIT).start();
			
		}catch(Exception e){IO.ERR(e.toString());}
		
		
		IO.INFO("total time: " + ((int)elapsedTime/10)/100.0 + " s");
		IO.INFO("Please wait for the cross models' plot to be plotted...");

		
	}  
}