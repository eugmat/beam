package BEAM2;


class MotifMemory {
	private MotifHandler handlerMemory;
	private double score;
	
	public MotifMemory(){
		this.handlerMemory = new MotifHandler();
		this.score = 0.0;
	}
	
	MotifMemory(double score_, MotifHandler mh_) {
		this.handlerMemory = new MotifHandler();
		this.handlerMemory.MotifHandlerClone(mh_);
		
		this.score = score_;
	}

	public void finalize(){
		
	}
	
	boolean tryMask(double score_, MotifHandler mh_, boolean cIRC){
		//se lo score e' maggiore di quello in memoria, salva quell'mh
		if(score_ > this.score){
			this.handlerMemory.MotifHandlerClone(mh_);
			this.score = score_;
			return true;
		}else{
			return false;
		}
	}
/*	
	public void applyMask(ArrayList<Motif> inputSequences, String name, int start, int end){
		
	}
*/
	public MotifHandler getHandlerMemory() {
		return handlerMemory;
	}

	public void setHandlerMemory(MotifHandler handlerMemory) {
		this.handlerMemory = handlerMemory;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
}
