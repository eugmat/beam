package BEAM2;

import java.util.ArrayList;

class Debug {
	private Debug() {}

	public static final boolean ON = false;
	//	public static final boolean ON = true;


	public static boolean VERBOSE = false;
	
	static void setVERBOSE(boolean state){
		VERBOSE = state;
	}

	static boolean checkStartEnd_element(Motif m, int motifWidth){
		return (m.getMotifEnd()-m.getMotifStart()) == motifWidth; 
	}

	static void checkRNA(MotifHandler mh) {
		// controlla se ci sono due rna con lo stesso nome
		ArrayList<String> rna = new ArrayList<String>();
		for (Motif m: mh.getListMotif()){
			if (rna.contains( m.getName() )  ){
				System.out.println(mh.toString());
				IO.scream(-1);
			}else{
				rna.add(m.getName());
			}
		}

	}
}
