package BEAM2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SearchStandalone {
	// Cerca args[0](benchmark) in args[1](fasta)

	public static void readPSSMfromBenchmark(String path, MotifHandler mh){
		FileReader f = null;

		try{
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader b = new BufferedReader(f);
			String s=b.readLine();

			while (!(s.isEmpty())){
				if(!s.split("\t")[1].split("\\$")[0].endsWith("_bg")){ //if not bg (if using search)
					mh.addMotif(s.split("\t")[1], "", s.split("\t")[0], 0, s.split("\t")[0].length());
				}
				s=b.readLine();
			}

		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	public static void main(String[] args) {
		//cerca args[0] (PSSM) in args[1] (query)
	
		long startTime = System.currentTimeMillis();
	
		//DONE 1 - con questa modifica la lettura di mbr passa da 100ms a 3ms -> su 10^6 calls
		// risparmia ~ 1g 
		double[][]mbr=SubsMat.getmbr();
		
	
		
		//IO.INFO("Reading query sequences...");
	
		ArrayList<Motif> querySequences=new ArrayList<Motif>();
	
		IO.readInputForSearch(args[1],querySequences); //riempie querySequences (i fasta in cui cercare)
		//TODO upgradabile (risparmio di qualche ora per ora)
	
	
		MotifHandler mh = new MotifHandler();
		double min = 0.0;
		if(args.length >= 3){
			min = Double.parseDouble(args[2]);
		}
		//IO.INFO("Reading benchmark file...");
		readPSSMfromBenchmark(args[0], mh);
	
		//IO.INFO("getting motif width...");
		mh.setMotifWidth();		
	
		//IO.INFO("Loading PSSM...");
		PSSM.computePSSM(mh, new ArrayList<Motif>());
		//TODO analizzare
	
		//IO.INFO("Search started on query.");
		//
		double bestMatch = 0.0;
		int counter = 0;
		for(Motif m: querySequences){
			bestMatch=0.0;
			//bisogna ciclare su le sequenze di querySequences (e per ogni sequenza su ogni subopt)
			//ciclo su sequenze
			if (m.getStructure().length() >= mh.getMotifWidth()){
				bestMatch=MotifUtilities.computeScoreVsPSSM(mh.getPSSM(), m, mbr, mh.getMotifWidth());
				//bestMatch = BEARManager.searchMotifUsingPSSMForSearch(mh.getPSSM(), m.getSequenceList(), mbr, mh.getMotifWidth());
				if (bestMatch > min){
					System.out.println(">" + m.getName());
					System.out.println(m.extractMotifFromStructure() + "\t" + m.getMotifStart() + "\t" + m.getMotifEnd() + "\t" + bestMatch);
					counter++;
				}
			}
		}
//		IO.INFO("\n#Search completed");
//		IO.INFO("#query over threshold (" + min + "): " + counter);
//		IO.INFO("#total query: " + querySequences.size() + " sequences.");
	
	
		long stopTime = System.currentTimeMillis();
		///*
		long elapsedTime = stopTime - startTime;
		//System.out.print("elapsed Time(ms):\t" + elapsedTime);
		//*/
	}
}
