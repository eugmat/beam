package BEAM2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.stat.inference.MannWhitneyUTest;
import org.apache.commons.math3.stat.inference.TTest;

class MotifUtilities {

	static void computePartials(PSSM pssm, MotifHandler mh, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		for (Motif m: mh.getListMotif()){
			//per ogni sequence in mh
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

				for(PSSMCell pos:pssm.getMatrix().get(j)){
					tmp+=BEARManager.substitutionScore(pos.name, m.extractMotifFromStructure().charAt(j), matrix)*pos.occurrence;
				}
			}
			double partial_ = tmp;
			m.setPartial(partial_);
		}
	}
	
	public static void computePartials_allSubopt(PSSM pssm, MotifHandler mh, double[][] matrix, int motifWidth) {
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		//This travels through all subopts and sets the best
		for (Motif m: mh.getListMotif()){
			//per ogni sequence in mh
			double tmp=0.0;
			for(String subopt: m.getStructureList()){
				

				for(int j=0; j < motifWidth;j++){
					//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

					for(PSSMCell pos:pssm.getMatrix().get(j)){
						tmp+=BEARManager.substitutionScore(pos.name, subopt.charAt(j), matrix)*pos.occurrence;
					}
				}
			}
			double partial_ = tmp;
			m.setPartial(partial_);
		}
		
	}

	static void computePartials_Nucleotides(PSSM pssm, MotifHandler mh, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		for (Motif m: mh.getListMotif()){
			//per ogni sequence in mh
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

				for(PSSMCell pos:pssm.getMatrix().get(j)){
					tmp+=BEARManager.substitutionScore_Nucleotides(pos.name, m.extractMotifFromNucleotides().charAt(j), matrix)*pos.occurrence;
				}
			}
			double partial_ = tmp;
			m.setPartial(partial_);
		}
	}

	static double computeScoreVsPSSM(PSSM pssm, Motif m, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		int idx=0;
		double max=-9999.0;
				
		for(int i=0; i < (m.getStructure().length()-motifWidth+1); i++ ){
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

				for(PSSMCell pos:pssm.getMatrix().get(j)){
					tmp+=BEARManager.substitutionScoreWithMask(pos.name, m.getStructure().charAt(j+i), matrix)*pos.occurrence;

				}
			}
			//tmp2 contiene il punteggio della finestra
			if(tmp > max){
				max=tmp;
				idx=i;
			}
		}
		if(m.setMotifStart(idx, motifWidth)){
			m.setMotifEnd(idx+motifWidth, motifWidth);
		}
		double partial_ = max;
		return partial_;

	}

	static double computeScoreVsPSSM_allSubopt(PSSM pssm, Motif m, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		int idx=0;
		int subIdx=0;
		double max=-9999.0;
		for(int k=0; k< m.getStructureList().size(); k++){ //for every subopt
			for(int i=0; i < (m.getStructure(k).length()-motifWidth+1); i++ ){
				double tmp=0.0;
				for(int j=0; j < motifWidth;j++){
					//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 
		
					for(PSSMCell pos:pssm.getMatrix().get(j)){
						tmp+=BEARManager.substitutionScoreWithMask(pos.name, m.getStructure(k).charAt(j+i), matrix)*pos.occurrence;
		
					}
				}
				//tmp2 contiene il punteggio della finestra
				if(tmp > max){
					max=tmp;
					subIdx=k;
					idx=i;
				}
			}
		}
		m.setIndex(subIdx);
		if(m.setMotifStart(idx, motifWidth)){
			m.setMotifEnd(idx+motifWidth, motifWidth);
		}
		double partial_ = max;
		return partial_;
	
	}

	static double computeScoreVsPSSM_Nucleotides(PSSM pssm, Motif m, double[][] matrix, int motifWidth){
		//input PSSM,mh. Il parziale � lo score della sequenza con la pssm, passi successivi: tenere solo quelli vicino alla media
		int idx=0;
		double max=-9999.0;
				
		for(int i=0; i < (m.getNucleotides().length()-motifWidth+1); i++ ){
			double tmp=0.0;
			for(int j=0; j < motifWidth;j++){
				//				scorri ogni colonna e calcola lo score come la media pesata per le occorrenze 

				for(PSSMCell pos:pssm.getMatrix().get(j)){
					try{
						tmp+=BEARManager.substitutionScoreWithMask_Nucleotides(pos.name, m.getNucleotides().charAt(j+i), matrix)*pos.occurrence;
					}catch(ArrayIndexOutOfBoundsException exc){
						IO.INFO("Are you sure your characters are all correct?\nIn:");
						IO.INFO(m.getName());
						System.exit(2);
					}
				}
			}
			//tmp2 contiene il punteggio della finestra
			if(tmp > max){
				max=tmp;
				idx=i;
			}
		}
		if(m.setMotifStart(idx, motifWidth)){
			m.setMotifEnd(idx+motifWidth, motifWidth);
		}
		//it could return false and not set the motif end (if masked)
		double partial_ = max;
		return partial_;

	}

	
	static public double max(double[] array){
		double max = -999.0;
		for(int i=0;i<array.length; i++){
			if (array[i] > max){
				max = array[i];
			}
		}
		return max;
	}

	public static void clean(MotifHandler mh, double thr, ArrayList<Motif> inputSequences){
		double mediaPart = 0.0;
		for(Motif m: mh.getListMotif()){ //questa parte puo' essere integrata al computePartials e passato direttamente la media qua
			if (m.getPartial() != Double.NEGATIVE_INFINITY ){
				mediaPart += m.getPartial();
			} //Al massimo poi regolare il denominatore con quelli tolti, ma non dovrebbe accadere comunque, ora dovrebbe succedere solo nel search successivo alla run, quindi si controlla da la
		}
		mediaPart /= mh.cardinality();
		//Ho ottenuto la media dei parziali, ora identifico quelli con punteggio meno del thr% della media
		IO.INFO("mean Partial:" + mediaPart);
		IO.INFO("Everything under " + mediaPart*thr + " will be removed");
		for(int i=(mh.cardinality()-1); i >=0 ; i--){
			if (mh.getObjectMotif(i).getPartial() < thr*mediaPart){
				//IO.ERR(mh.getObjectMotif(i).getPartial());
				inputSequences.add(mh.getObjectMotif(i));
				mh.removeMotif(i);
				
			}else{
				//IO.INFO(mh.getObjectMotif(i).getPartial());
			}
		}
	}

	static public int maxIndex(double[] array){
		double max = -999.0;
		int maxI = 0;
		for(int i=0;i<array.length; i++){
			if (array[i] >= max){
				max = array[i];
				maxI = i;
			}
		}
		return maxI;
	}

	static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
		if ( x > 0) {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
		} else {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
		}
	}

	static void setBranchingZero(double[][] mbr){
		mbr[48][48]=0.0;
	}

	static void reshapeInput(ArrayList<Motif> inputSequences) {
		//controlla inputSequences se ci sono duplicati causati da boh. In caso toglili
		//Da lanciare una volta a inizio run
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<Motif> toBeRem = new ArrayList<Motif>();
		for (Motif m: inputSequences){
			if (list.contains(m.getName()) ){
				toBeRem.add(m);
			}else{
				list.add(m.getName());
			}
		}
		for (Motif m: toBeRem){
			inputSequences.remove(m);
		}

	}

	static double computeMeanLength(ArrayList<Motif> inputSequences) {
		// fornisce lunghezza media delle sequenze in inputSequences, per determinare LSBin
		double mean=0.0;
		for(Motif m: inputSequences){
			//TODO in caso di subopt gestire questa cosa
			mean+=m.getStructure().length();
		}
		return mean/inputSequences.size();
	}

	static double computeMeanStructureContent(
			ArrayList<Motif> inputSequences) {
		// fornisce struttura media delle sequenze in inputSequences, per determinare LSBin
		double mean=0.0;
		for(Motif m:inputSequences){
			//TODO in caso di subopt gestire questa cosa
			mean+=assessStructureContent(m.getStructure());
		}
		return mean/inputSequences.size();
	}

	private static double assessStructureContent(String bear){
		double sc=0.0;
		String stemClass = "abcdefghi=ABCDEFGHIJ";
		for (int i=0; i<bear.length();i++){
			if(stemClass.contains(Character.toString(bear.charAt(i)) )  ){
				sc+=1.0;
			}
		}
		return sc/bear.length();
	}

	static double computePvalue(String gaussFile, double meanPart, KS ks, double coverage) throws NoDataException {

		// dato file Gauss + media parziali calcola pvalue 1-tailed
		double pvalue=-2;
		double statistic=-1.0;
		double t=-9999.0;
		double mean=0.0;
		double std=0.0;
		
		FileReader f = null;
		String s="";
		
		try{
			//apro file
			f = new FileReader(gaussFile);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			while ((s=reader.readLine())!=null){
				if (s.startsWith("mean")){
					mean=Double.parseDouble(s.split("\t")[1]);
				}else if(s.startsWith("var")){
					std = Math.sqrt(Double.parseDouble(s.split("\t")[1]));
				}
			}
		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//convert arraylists in arrays
		ArrayList<Double> obs = ks.getForeground_partials();
		ArrayList<Double> best_obs = ks.getBest_fore_partials();

//		Collections.sort(obs);
//		Collections.reverse(obs);
		
		double[] target_fore = new double[obs.size()];
		for (int i = 0; i < target_fore.length; i++) {
			target_fore[i] = obs.get(i);               
		}
		
		double[] target_best_fore = new double[best_obs.size()];
		for (int i = 0; i < target_best_fore.length; i++) {
			target_best_fore[i] = best_obs.get(i);               
		}
		
		ArrayList<Double> obs_bg = ks.getBackground_partials();
//		Collections.sort(obs_bg);
//		Collections.reverse(obs_bg); //serviva per selezionare le top istanze, ora posso pensare di usarlo per la visualizzazione in .search.txt
		
		//int corrected_size; //corrections to take top coverage bg
		//double[] target_back = new double[(int)(obs_bg.size()*coverage)]; 
		
		double[] target_back = new double[(int)(obs_bg.size())]; //taking everything instead
		Collections.sort(obs_bg);
		Collections.reverse(obs_bg);
		for (int i = 0; i < target_back.length; i++) {
			target_back[i] = Double.parseDouble(String.format("%.4f", obs_bg.get(i)));               
		}
		
		//MannWhitney is calculated over threshold for input and on equal sized background (best picks)
		double[] target_best_back = new double[(int)(obs_bg.size()*coverage)];
		for (int i = 0; i < target_best_back.length; i++) {
			target_best_back[i] = Double.parseDouble(String.format("%.4f", obs_bg.get(i)));               
		}
		//
		
		//Effect Size
		double cohen=(meanPart-mean)/std;
		ks.setCohen(cohen);
		NormalDistribution gauss;
		
		try{
		gauss= new NormalDistribution(mean, std);
		}catch(Exception e){
			gauss= new NormalDistribution(mean, 0.01);		
		}
		
		double cohenu3 = gauss.cumulativeProbability(meanPart);
		ks.setCohenu3(cohenu3);

		//MannWhitney, this is calculated on over thr (defined by the model) instances, thus assessing classification power of the motif
		MannWhitneyUTest U = new MannWhitneyUTest();
		try{
		double ustat = U.mannWhitneyU(target_best_back, target_best_fore);
		double up = U.mannWhitneyUTest(target_best_back, target_best_fore);
		double aucmw = ustat/(best_obs.size()*target_best_back.length);

		ks.setpMW(up);
		ks.setStatisticMW(ustat);
		ks.setAUCMW(aucmw);
		}catch(NoDataException e){
			e.printStackTrace();
		}
		//t-test 
		TTest studentTest =  new TTest();
		try{
		pvalue = studentTest.tTest(target_fore, target_back);
		t = studentTest.t(target_fore, target_back);
		ks.setPvalue(pvalue);
		ks.setStatistic(t);
		}catch(Exception e){
			IO.ERR("there was an error trying to calculate t statistic!");
			e.printStackTrace();
			return -1;
		}
		
		//HG statistic
		HypergeometricDistribution hg = new HypergeometricDistribution(
				ks.getBackground_partials().size()+ks.getForeground_partials().size(),
				ks.getForeground_partials().size(),
				ks.getSampleSize());
		int successes=ks.getBest_fore_partials().size();
		//TODO put Enrichment too?
		double phg=hg.upperCumulativeProbability(successes);
		ks.setpHG(phg);
		System.out.println(phg);
		
		return 1;
	}
	
	
	
	static void applyMask(MotifHandler mh){
		for (Motif m: mh.getListMotif()){
			m.addMask(m.getMotifStart(), m.getMotifEnd());
		}
	}
	
	static void applyMaskCirc(MotifHandler mh, int maxWidth){
		for (Motif m: mh.getListMotif()){
			int extension= Math.min(m.getDotBracket().length(), Math.max(50, maxWidth));
			//dotbracket is left untouched (original length)
			int start = m.getMotifStart();
			int end = m.getMotifEnd();
			int leng = m.getDotBracket().length();
			
			
			m.addMask(start, end);
			if(start <= extension & end <= extension){//1 all left
				m.addMask(start+leng, end+leng);
				m.addMask(start+2*leng, end+2*leng);
			}else if(start <= extension & end > extension & end <= extension+leng){//4 circular first junction
				m.addMask(start+leng, end+leng);
				
			}else if(start <= extension+leng & end <= extension+leng){//2 middle
				m.addMask(start+leng, end+leng);
				m.addMask(start-leng, end-leng);
			}else if(start > extension & start <= extension+leng & end > extension+leng){//5 circular second junction
				m.addMask(start-leng, end-leng);
			}else if(start > extension+leng & end > extension+leng){//3 right
				m.addMask(start-leng, end-leng);
				m.addMask(start-2*leng, end-2*leng);

			}
			correctMask(m);

		}
		
	}
	
	
	
	private static void correctMask(Motif m) {
		//if after CIRC, a mask is outside the boundaries, bring them back to the boundaries.
		ArrayList<int[]> removable = new ArrayList<int[]>();

		for(int i=0; i<m.getMaskList().size(); i++){ //every subopt
			for(int j=0; j<m.getMaskList().get(i).size(); j++){//every mask
				if(m.getMaskList().get(i).get(j)[0] < 0){ // if start < 0
					if(! (m.getMaskList().get(i).get(j)[1] < 0)){ //if end > 0 otherwise TODO remove mask??
						m.setMask(i, j, new int[]{0,m.getMaskList().get(i).get(j)[1]});
					}else{
						removable.add(new int[]{i,j});

					}
				}
				if(m.getMaskList().get(i).get(j)[1] > m.getStructure().length()){
					if(! (m.getMaskList().get(i).get(j)[0] > m.getStructure().length())){
						m.setMask(i, j, new int[]{m.getMaskList().get(i).get(j)[0],m.getStructure().length()-1 });
					}else{ 
						removable.add(new int[]{i,j});
					}
				}
			}

		}
		for (int k=removable.size(); k>0; k-- ){
			m.removeMask(removable.get(k-1)[0], removable.get(k-1)[1]);
		}

	}

	static void circularize(ArrayList<Motif> inputSequences, int maxWidth) {
		// extend sequences and structures to the left and to the right of min(seqLen||max(50||W)) 
		int extension=Math.max(50, maxWidth);
		for(Motif m: inputSequences){ //for the nucleotides
			extension = Math.min(extension, m.getStructure().length());
			m.setNucleotides(m.getNucleotides().substring(m.getNucleotides().length()-extension)+m.getNucleotides()+m.getNucleotides().substring(0,extension));
			for(int i=0; i<m.getStructureList().size(); i++){ //for each subopt
				m.setStructure(m.getStructure(i).substring(m.getStructure(i).length()-extension)+m.getStructure(i)+m.getStructure(i).substring(0,extension), i);
			}
		
		}
		IO.INFO("circularized!");
	}

	static int decircularize(int coordinate, int extension, int leng) {
		// brings the coordinate back to its original reference system
		if(coordinate < extension){
			coordinate = coordinate+ leng-extension;
		}else if (coordinate < leng+extension){
			coordinate -= extension;
		}else{
			coordinate -= (extension+leng);
		}
		return coordinate;
	}
	


}
