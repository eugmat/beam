package BEAM2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class PSSM {
	private ArrayList<ArrayList<PSSMCell>> matrix;
	private double score;

	public PSSM(){	
		this.matrix=new ArrayList<ArrayList<PSSMCell>>();
		this.score=0.0;
	}

	public void setScore(double d){
		this.score=d;
	}

	public double getScore(){
		return this.score;
	}

	public int getSize(){
		return matrix.size();
	}

	public ArrayList<ArrayList<PSSMCell>> getMatrix(){
		return this.matrix;
	}
	

	static void computePSSM(MotifHandler mh, ArrayList<Motif> inputSequences){
		//calcola pssm di un allineamento locale di finestre, o motifHandler
		// in questa versione se una delle sequenze ha gli indici sballati la toglie

		mh.getPSSM().matrix=new ArrayList<ArrayList<PSSMCell>>();
		int total=mh.cardinality();

		//mh.setMotifWidth();
		

		
		for(int j=0;j<mh.getMotifWidth();j++){
			ArrayList<PSSMCell> aL=new ArrayList<PSSMCell>();
			for(int i =0; i<total;i++){
				//TODO la prima condizione � un controllo per quando sballa gli indici, andr� risolto.
				if ((mh.getObjectMotif(i).getMotifEnd() - mh.getObjectMotif(i).getMotifStart()) > 0 && mh.getStructureMotif(i).length() == mh.getMotifWidth() ){
					if(aL.isEmpty()){
						PSSMCell tmp=new PSSMCell(mh.getStructureMotif(i).charAt(j), 1.0/total);
						aL.add(tmp);
					}else{
						boolean found=false;
						for(PSSMCell chiave:aL){
							if(chiave.name == mh.getStructureMotif(i).charAt(j)){
								chiave.occurrence+=1.0/total;
								found=true;
							}
						}
						if(!found){
							PSSMCell tmp=new PSSMCell(mh.getStructureMotif(i).charAt(j),1.0/total);
							aL.add(tmp);	
						}
					}
				}else{
					//Patch, se la sequenza sta per sballare, rimettila via e apposto
					
					inputSequences.add(mh.getObjectMotif(i));
					mh.getListMotif().remove(i);
					i--;
					total--;
				}
			}
			mh.getPSSM().matrix.add(aL);
		}
		if (Debug.VERBOSE){
		IO.INFO("[calculating PSSM...]");
		IO.INFO("[processing " + total + " sequences...]");
		IO.INFO("[motif width: "+ mh.getMotifWidth() +"]");
		}

	}
	
	static void computePSSM_Nucleotides(MotifHandler mh, ArrayList<Motif> inputSequences){
		//calcola pssm di un allineamento locale di finestre, o motifHandler (SEQUENCES)
		// in questa versione se una delle sequenze ha gli indici sballati la toglie

		mh.getPSSM().matrix=new ArrayList<ArrayList<PSSMCell>>();
		int total=mh.cardinality();

		mh.setMotifWidth();
		
		
		for(int j=0;j<mh.getMotifWidth();j++){
			ArrayList<PSSMCell> aL=new ArrayList<PSSMCell>();
			for(int i =0; i<total;i++){
				//TODO la prima condizione � un controllo per quando sballa gli indici, andr� risolto.
				if ((mh.getObjectMotif(i).getMotifEnd() - mh.getObjectMotif(i).getMotifStart()) > 0 && mh.getNucleotideMotif(i).length() == mh.getMotifWidth() ){
					if(aL.isEmpty()){
						PSSMCell tmp=new PSSMCell(mh.getNucleotideMotif(i).charAt(j), 1.0/total);
						aL.add(tmp);
					}else{
						boolean found=false;
						for(PSSMCell chiave:aL){
							if(chiave.name == mh.getNucleotideMotif(i).charAt(j)){
								chiave.occurrence+=1.0/total;
								found=true;
							}
						}
						if(!found){
							PSSMCell tmp=new PSSMCell(mh.getNucleotideMotif(i).charAt(j),1.0/total);
							aL.add(tmp);	
						}
					}
				}else{
					//Patch, se la sequenza sta per sballare, rimettila via e apposto
					
					inputSequences.add(mh.getObjectMotif(i));
					//this references only to the Motif Object, not the motif itself (independent from nucleotides/structure)
					mh.getListMotif().remove(i);
					i--;
					total--;
				}
			}
			mh.getPSSM().matrix.add(aL);
		}
		

	}
	
	//************FINE PSSM NEW SCORE****************


	//*************SCORE CON PARZIALI*************
	static double ScorePartials(MotifHandler mh){ //base, senza bonus seq, non utilizzato
		double score = 0.0;
		for (Motif m:mh.getListMotif()){
			score += m.getPartial();
		}
		return score;
	}

	private class CustomComparator implements Comparator<PSSMCell> {

		@Override
		public int compare(PSSMCell arg0, PSSMCell arg1) {
			//Implementato al contrario per avere un reverse sort ;) 
			if(arg0.occurrence - arg1.occurrence > 0){
				return -1;
			}else if(arg0.occurrence - arg1.occurrence < 0){
				return 1;
			}else{
				return 0;
			}
		}
	}

	String toString(int motifWidth){
		String out="";
		for(int j=0;j<motifWidth;j++){
			Collections.sort(this.matrix.get(j), new CustomComparator());
			for(PSSMCell chiave:this.matrix.get(j) ){
				out+=chiave.name+" : "+MotifUtilities.truncateDecimal(chiave.occurrence,2)+"\t";
			}
			out+="\n";
		}
		return out;
	}
	



	public static void computeQBEARPSSM(MotifHandler mh){
		mh.getQBEARPSSM().matrix=new ArrayList<ArrayList<PSSMCell>>();
		int total=mh.cardinality();
		for(int j=0;j<mh.getMotifWidth();j++){	//su ogni carattere del motivo
			ArrayList<PSSMCell> aL=new ArrayList<PSSMCell>();
			for(int i =0; i<total;i++){ //per ogni sequenza
				if(aL.isEmpty()){
					PSSMCell tmp=new PSSMCell(qBEAR.identifyClass(mh.getStructureMotif(i).charAt(j) ), 1.0/total);
					aL.add(tmp);
				}else{
					boolean found=false;
					for(PSSMCell chiave:aL){
						//IO.INFO(this.motifList.get(i).getMotifStart()+"\t"+this.motifList.get(i).getMotifEnd()+"\t"+this.motifList.get(i).getSequence());
						if(chiave.name == qBEAR.identifyClass(mh.getStructureMotif(i).charAt(j))){
							chiave.occurrence+=1.0/total;
							found=true;
						}
					}
					if(!found){
						PSSMCell tmp=new PSSMCell(qBEAR.identifyClass(mh.getStructureMotif(i).charAt(j)),1.0/total);
						aL.add(tmp);	
					}
				}
			}
			mh.getQBEARPSSM().matrix.add(aL);
		}
	}



}
