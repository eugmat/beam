package BEAM2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.io.FilenameUtils;

class MutualInfo {

	private static ArrayList<String> listModelsForFolder(final File folder) {
		//needs a final File folder = new File("/home/you/Desktop");
		ArrayList<String> models=new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listModelsForFolder(fileEntry);
			} else {
				if(fileEntry.getName().endsWith("search.txt") || fileEntry.getName().endsWith("search.nuc.txt")    ){
					models.add(fileEntry.getName());
				}
			}
		}

		return models;
	}

	private static ArrayList<String> getPairs(ArrayList<String> models){
		//generate all unique pairs (even a:b b:a) in ArrayList<String>; returns ArrayList with elements "a:b"
		ArrayList<String> pairs=new ArrayList<String>();
		for (int i=0; i< models.size(); i++){
			for (int j=0; j<models.size(); j++){
				pairs.add(models.get(i)+":"+models.get(j));
			}
		}

		return pairs;
	}

	private static Double mi(ArrayList<String> ids1, ArrayList<String> ids2, ArrayList<String> IDs) throws FileNotFoundException, IOException {
		// compute Mutual Information between two motif models - ( lists of strings containing IDs)
		Double MI=0.0;
		
//		for (String s: IDs){
//			IO.INFO(s);
//		}
		
		ArrayList<Integer> ids1bin = binarization(ids1,IDs);
		ArrayList<Integer> ids2bin = binarization(ids2,IDs);
		
//		for (Integer in: ids1bin){
//			IO.ERR(in);
//		}
		Double p1,p2=0.0;
		p1= Collections.frequency(ids1bin, 1)/(double)ids1bin.size(); //p1_1
		p2= Collections.frequency(ids2bin, 1)/(double)ids1bin.size(); //p2_1
		
		ArrayList<Integer> pa = pairs_binarization(ids1bin,ids2bin);
		double tot = (double) ids1bin.size();
//		IO.INFO(p1);
//		IO.INFO(p2);
		
		//some problems, not convinced enough
//		MI_ += 
//				- p1*Math.log(p1) - (1-p1)*Math.log(1-p1)
//				- p2*Math.log(p2) - (1-p2)*Math.log(1-p2)
//				+ p1*p2*Math.log(p1*p2) + p1*(1-p2)*Math.log(p1*(1-p2)) + (1-p1)*p2*Math.log((1-p1)*p2) + (1-p1)*(1-p2)*Math.log((1-p1)*(1-p2));
		Double pij = pa.get(0)/tot;
		Double pi = p1;
		Double pj = p2;
		MI -= pij*Math.log(pi*pj);
		pij = pa.get(1)/tot;
		pi = p1;
		pj = 1-p2;
		MI -= pij*Math.log(pi*pj);
		pij = pa.get(2)/tot;
		pi = 1-p1;
		pj = p2;
		MI -= pij*Math.log(pi*pj);
		pij = pa.get(3)/tot;
		pi = 1-p1;
		pj = 1-p2;
		MI -= pij*Math.log(pi*pj);
		MI /=Math.log(2);
		
		return MI;

	}

	
	private static ArrayList<Integer> binarization(ArrayList<String> ids, ArrayList<String> refIDs){
		//returns an aL with 0s and 1s. 1 if refID in id, 0 otherwise
		ArrayList<Integer> binary= new ArrayList<Integer>();

		for (String el: refIDs){
			if (ids.contains(el)){
				binary.add(1);
			}else{
				binary.add(0);
			}

		}
		return binary;
	}
	
	private static ArrayList<Integer> pairs_binarization(ArrayList<Integer> ids1bin, ArrayList<Integer> ids2bin){
		//returns the number of 11,10,01,00
		int oneone=0,onezero=0,zeroone=0,zerozero=0;

		for (int i = 0; i < ids1bin.size(); i++){
			if (ids1bin.get(i)==1 && ids2bin.get(i)==1) oneone += 1;
			else if (ids1bin.get(i)==1 && ids2bin.get(i)==0) onezero += 1;
			else if (ids1bin.get(i)==0 && ids2bin.get(i)==1) zeroone += 1;
			else zerozero += 1;
		}
		ArrayList<Integer> mi_count=new ArrayList<Integer>();
		mi_count.add(oneone);
		mi_count.add(onezero);
		mi_count.add(zeroone);
		mi_count.add(zerozero);

		return mi_count;
	}
	
	private static Double Concurrency(ArrayList<String> ids1, ArrayList<String> ids2){
		Double conc=0.0;
		int size_t0=ids1.size();
		ids1.removeAll(ids2);
		int size_t=ids1.size();
		conc =((size_t0-size_t)/(double)size_t0);
		return conc;
	}
	private static Double Cocoverage(ArrayList<String> ids1, ArrayList<String> ids2, int size){
		Double coco=0.0;
		int size_t0=ids1.size(); //model 1 IDs
		ids1.removeAll(ids2);
		int size_t=ids1.size(); //model1-model2 IDs
		//model1 AND model2 size % inputsize
		coco =((size_t0-size_t)/(double)size);
		return coco;
	}

	private static ArrayList<String> cleanBackground(String root, String model) throws FileNotFoundException, IOException{
		//reads model_search file and returns list of input RNAs involved in model
		ArrayList<String> ids = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(FilenameUtils.concat(root, model)))) {
			String line;
			while ((line = br.readLine()) != null) {
				// process the line.
				if (line.startsWith("#otherMatches")){
					break;
				}else{
					String[] chunk=line.split("\t");
					if (chunk.length > 1 && !chunk[1].split("\\$")[0].endsWith("_bg")){
						ids.add(chunk[1].split("\\$")[0]);
					}
				}
			}
		}
		return ids;

	}
	




	static void postAnalysis(String motifFolder, String inputFile) throws FileNotFoundException, IOException{
		//method to call from outside, writes the analysis file
		final File folder = new File(motifFolder);
		ArrayList<String> models = listModelsForFolder(folder);
		ArrayList<String> pairs = getPairs(models);
		String fileName=motifFolder.split("/")[motifFolder.split("/").length-3];
		String pathAnalysis=FilenameUtils.concat(motifFolder, "cross_analysis.txt");
		
		for(String pair : pairs){
			IO.INFO(pair);
		}
		IO.INFO("");
		
		
		//concurrency
		writeCooccurrence(motifFolder, models, pairs, fileName, pathAnalysis);
		ArrayList<String> IDs = new ArrayList<String>();
		IO.readInputIDs(inputFile, IDs);
		writeCocoverage(motifFolder, models, pairs, fileName, pathAnalysis, IDs.size());

		writeMutualInfo(motifFolder, models, pairs, fileName, pathAnalysis, IDs);

		//distance distribution (use pairs of models to analyze distance composition)
		//Go python yourself (script in py)
	}

	private static void writeCooccurrence(String motifFolder, ArrayList<String> models, ArrayList<String> pairs,
			String fileName, String pathAnalysis) {
		IO.INFO("\nCooccurrency estimate the coverage of model A in model B (asymmetrical)\n");


		try {

			FileWriter writer = new FileWriter(pathAnalysis);	

			//
			writer.write("#"+fileName+"\n#Cooccurrence\n");
			String topRow="";
			String topRowtmp="";
			for(String mod_: models){
				
				topRowtmp += mod_.split("_")[mod_.split("_").length - 2].split("m")[1];
				if (mod_.endsWith(".search.nuc.txt")) topRowtmp ="sequence"+topRowtmp;
				else topRowtmp ="structure"+topRowtmp;

				topRow += "\t" + topRowtmp;
				topRowtmp="";
			}
			writer.write(topRow);


			String baserow="" ;
			String row="" ;

			for(String pair: pairs){

				String model1=pair.split(":")[0];
				String model2=pair.split(":")[1];
				ArrayList<String> ids1= cleanBackground(motifFolder, model1);
				ArrayList<String> ids2= cleanBackground(motifFolder, model2);

				IO.INFO(model1+"\t" + model2);
				
				BigDecimal conc=MotifUtilities.truncateDecimal(Concurrency(ids1,ids2),2);

				IO.INFO("cooccurrence:\t" + conc);

				if (baserow.equals("") || !baserow.equals(model1)){
					//if first element has not been written or if it has changed
					baserow=model1;
					writer.write(row);
					row="";
					String mod_="";

					mod_+= model1.split("_")[model1.split("_").length - 2].split("m")[1];
					if (model1.endsWith(".search.nuc.txt")) mod_="sequence"+mod_;
					else mod_="structure"+mod_;

					row="\n"+mod_;
				}

				row += "\t" + conc;
			}
			writer.write(row);

			writer.close();
		} catch (IOException ex) {
			IO.ERR("IOException: " + ex.getMessage());
		}
	}
	
	
	
	private static void writeCocoverage(String motifFolder, ArrayList<String> models, ArrayList<String> pairs,
			String fileName, String pathAnalysis, int size) {
		IO.INFO("\nCocoverage estimates the coverage of model A AND model B (symmetrical)\n");
	
	
		try {
	
			FileWriter writer = new FileWriter(pathAnalysis, true);	
	
			//
			writer.write("\n\n#Cocoverage\n");
			String topRow="";
			String topRowtmp="";
			for(String mod_: models){
				
				topRowtmp += mod_.split("_")[mod_.split("_").length - 2].split("m")[1];
				if (mod_.endsWith(".search.nuc.txt")) topRowtmp ="sequence"+topRowtmp;
				else topRowtmp ="structure"+topRowtmp;
	
				topRow += "\t" + topRowtmp;
				topRowtmp="";
			}
			writer.write(topRow);
	
	
			String baserow="" ;
			String row="" ;
	
			for(String pair: pairs){
	
				String model1=pair.split(":")[0];
				String model2=pair.split(":")[1];
				ArrayList<String> ids1= cleanBackground(motifFolder, model1);
				ArrayList<String> ids2= cleanBackground(motifFolder, model2);
	
				IO.INFO(model1+"\t" + model2);
				
				BigDecimal conc=MotifUtilities.truncateDecimal(Cocoverage(ids1,ids2,size),2);
	
				IO.INFO("cocoverage:\t" + conc);
	
				if (baserow.equals("") || !baserow.equals(model1)){
					//if first element has not been written or if it has changed
					baserow=model1;
					writer.write(row);
					row="";
					String mod_="";
	
					mod_+= model1.split("_")[model1.split("_").length - 2].split("m")[1];
					if (model1.endsWith(".search.nuc.txt")) mod_="sequence"+mod_;
					else mod_="structure"+mod_;
	
					row="\n"+mod_;
				}
	
				row += "\t" + conc;
			}
			writer.write(row);
	
			writer.close();
		} catch (IOException ex) {
			IO.ERR("IOException: " + ex.getMessage());
		}
	}

	private static void writeMutualInfo(String motifFolder, ArrayList<String> models, ArrayList<String> pairs,
			String fileName, String pathAnalysis, ArrayList<String> IDs) {
		IO.INFO("\nMI (symmetrical)\n");

		try {
			FileWriter writer_MI= new FileWriter(pathAnalysis, true);	


			writer_MI.write("\n\n#MI\n");
			String topRow="";
			String topRowtmp="";
			for(String mod_: models){
				topRowtmp += mod_.split("_")[mod_.split("_").length - 2].split("m")[1];
				if (mod_.endsWith(".search.nuc.txt")) topRowtmp ="sequence"+topRowtmp;
				else topRowtmp ="structure"+topRowtmp;				

				topRow += "\t" + topRowtmp;
				topRowtmp=""; 
			}
			writer_MI.write(topRow);


			String baserow="" ;
			String row="" ;

			for(String pair: pairs){

				String model1=pair.split(":")[0];
				String model2=pair.split(":")[1];
				ArrayList<String> ids1= cleanBackground(motifFolder, model1);
				ArrayList<String> ids2= cleanBackground(motifFolder, model2);
				
				BigDecimal MI = null;
				Double MI_= mi(ids1,ids2,IDs);
				if (!MI_.isNaN()) { MI=MotifUtilities.truncateDecimal(MI_,2);}

				IO.INFO(model1+"\t" + model2);
				IO.INFO("MI:\t" + MI_);

				if (baserow.equals("") || !baserow.equals(model1)){
					//if first element has not been written or if it has changed
					baserow=model1;
					writer_MI.write(row);
					row="";
					String mod_="";

					mod_+= model1.split("_")[model1.split("_").length - 2].split("m")[1];
					if (model1.endsWith(".search.nuc.txt")) mod_="sequence"+mod_;
					else mod_="structure"+mod_;
					row="\n"+mod_;
				}
				if (!MI_.isNaN()){
					row += "\t" + MI;
				}
				else{
					row += "\t" + MI_ ;
				
				}
			}
			writer_MI.write(row);

			writer_MI.close();
		} catch (IOException ex) {
			IO.ERR("IOException: " + ex.getMessage());
		}
	}


	public static void main(String[] args) throws FileNotFoundException, IOException{
		//test
		IO.INFO("mutual info main");

		IO.INFO(args[0]);

		final File folder = new File(args[0]);
		ArrayList<String> models = listModelsForFolder(folder);
		ArrayList<String> pairs = getPairs(models);

		IO.INFO("\nCooccurrence estimate the coverage of model A in model B (asymmetrical)\n");

		for(String pair: pairs){
			String model1=pair.split(":")[0];
			String model2=pair.split(":")[1];
			ArrayList<String> ids1= cleanBackground(args[0], model1);
			ArrayList<String> ids2= cleanBackground(args[0], model2);
			IO.INFO(model1+"\t" + model2);
			IO.INFO(Concurrency(ids1,ids2).toString());
		}
		//postAnalysis(args[0], new String());
		IO.INFO("mutual info main");

	}



}
