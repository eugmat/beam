package BEAM2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;

class IO {
	private static boolean KEEP=false;
	private static boolean INIT_SUMMARY=false;
	private static boolean INIT_SUMMARY_NUC=false;

	static void readInput(String path, ArrayList<Motif> inputSequences){
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;
			boolean subopt = false;

			while ((s=reader.readLine())!=null){

				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].replaceAll("[\r\n]",""); //perche sta cattiveria?
						start=false;
						//						if(firstSequence){ //se e' la prima subopt segnati il nome
						//							firstName = s.substring(1).split("�")[0];
						//							firstSequence = false;
						//						}

					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}

				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);

						if(subopt){ //se nel passo precedente e' stata riconosciuta una subopt allora non aggiungere un nuovo
							//Motif ad inputSequences, ma riempi l'arrayList di String dell'ultimo messo
							inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);

						}else{
							//aggiungi la subopt -0- a inputSequences
							inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
							firstName = name;
							inputSequences.get(inputSequences.size()-1).addSuboptMask();


						}



						name = s.substring(1).split("\\$")[0].replaceAll("[\r\n]", "");
						seq="";
						nuc="";
						dotB="";


						if(name.equals(firstName)){//se la nuova sequenza e' una subopt di quella sopra
							subopt = true;
						}else{
							subopt = false;
						}

					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}

				}
			}

			//quando esci segnati l'ULTIMA sequenza del file
			if(subopt){
				inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);
			}else{
				inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
				inputSequences.get(inputSequences.size()-1).addSuboptMask();

			}
		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	static void scream(int status){
		System.err.println("AAAAAAARGH!");
		System.exit(status);
	}

	static void INFO(String info){
		System.out.println("[INFO]" + info);
	}

	static void INFO(Integer info){
		System.out.println("[INFO]" + info.toString());
	}

	static void INFO(double info) {
		System.out.println("[INFO]" + (double)(info));		
	}

	static void ERR(String err){
		if(Debug.VERBOSE){
			System.err.println("[ERROR]" + err);
		}
	}

	public static void deleteFile(String myFile) throws IOException{
		File file = new File(myFile);
		FileDeleteStrategy.FORCE.delete(file);

	}

	public static void deleteFolder(String myDirectoryPath) throws IOException{
		File fin = new File(myDirectoryPath);
		//deletes all child of folder, not the parent
		for (File file : fin.listFiles()) {
			FileDeleteStrategy.FORCE.delete(file);
		}   
	}

	static void deleteFolder(String myDirectoryPath, boolean parent) throws IOException{
		//deletes folder
		File fin = new File(myDirectoryPath);
		File[] directoryListing = fin.listFiles();
		if(directoryListing != null){ 
			for (File file : fin.listFiles()) {
				FileDeleteStrategy.FORCE.delete(file);
			}   
			if (parent==true){
				FileDeleteStrategy.FORCE.delete(fin);
			}
		}


	}

	static void moveBestRuns(String baseName, int mask, int bestRunIdx,
			String outDad, String method) throws IOException {
		//copy best benchmark in final folder, use bestRunIdx to manage filenames basename_m<mask>_run<bestRunIdx>
		String sourcename= outDad +"/mask"+ mask + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".txt"; 
		String destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".txt"; 
		if (method=="sequence"){
			sourcename= outDad +"/mask"+ mask + "_nuc/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".nuc.txt"; 
			destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".nuc.txt"; 
		}

		File source = new File(sourcename);
		File dest = new File(destname);
		copyUsingFileChannels(source,dest);

	}

	static void moveBestRunsLogo(String baseName, int mask, int bestRunIdx,
			String outDad, String method) throws IOException {
		//copy best benchmark in final folder, use bestRunIdx to manage filenames basename_m<mask>_run<bestRunIdx>
		String sourcename= outDad +"/mask"+ mask + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.fa"; 
		String destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.fa"; 
		if (method=="sequence"){
			sourcename= outDad +"/mask"+ mask + "_nuc/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.nuc.fa"; 
			destname=outDad +"/motifs/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.nuc.fa"; 
		}

		File source = new File(sourcename);
		File dest = new File(destname);
		copyUsingFileChannels(source,dest);

	}

	static void copyUsingFileChannels(File source, File dest) throws IOException {
		try {
			FileUtils.copyFile( source, dest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void maskedExit() {
		// TODO Auto-generated method stub
		System.out.println("No unmasked sequences left");
		return;
	}

	static void readInputForSearch(String path, ArrayList<Motif> inputSequences){
		//riempie inputSequences di ci� che c'� in path. 
		//DOVREBBE prendere anche inputSequences parzialmente riempiti
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;
			boolean subopt = false;

			while ((s=reader.readLine())!=null){

				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].replaceAll("[\r\n]","");
						start=false;
						//						if(firstSequence){ //se e' la prima subopt segnati il nome
						//							firstName = s.substring(1).split("�")[0];
						//							firstSequence = false;
						//						}

					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}

				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);

						if(subopt){ //se nel passo precedente e' stata riconosciuta una subopt allora non aggiungere un nuovo
							//Motif ad inputSequences, ma riempi l'arrayList di String dell'ultimo messo
							inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);

						}else{
							//aggiungi la subopt -0- a inputSequences
							inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
							firstName = name;
							inputSequences.get(inputSequences.size()-1).addSuboptMask();


						}



						name = s.substring(1).split("\\$")[0].replaceAll("[\r\n]", "");
						seq="";
						nuc="";
						dotB="";


						if(name.equals(firstName)){//se la nuova sequenza e' una subopt di quella sopra
							subopt = true;
						}else{
							subopt = false;
						}

					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}

				}
			}

			//quando esci segnati l'ULTIMA sequenza del file
			if(subopt){
				inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);
			}else{
				inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
				inputSequences.get(inputSequences.size()-1).addSuboptMask();

			}
		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	static void readInputForSearch(String path, ArrayList<Motif> inputSequences, boolean bgFlag){
		//riempie inputSequences di ci� che c'� in path. 
		//prende anche inputSequences parzialmente riempiti
		//VERSIONE background, appende _bg a tutte le sequenze, in modo da riconoscerle
		//per l'MCC (o altre analisi a posteriori)
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(path);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;
			boolean subopt = false;

			while ((s=reader.readLine())!=null){

				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].replaceAll("[\r\n]","") + "_bg";
						start=false;

					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}

				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);

						if(subopt){ //se nel passo precedente e' stata riconosciuta una subopt allora non aggiungere un nuovo
							//Motif ad inputSequences, ma riempi l'arrayList di String dell'ultimo messo
							inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);

						}else{
							//aggiungi la subopt -0- a inputSequences
							inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
							firstName = name;
							inputSequences.get(inputSequences.size()-1).addSuboptMask();


						}



						name = s.substring(1).split("\\$")[0].replaceAll("[\r\n]", "") + "_bg";
						seq="";
						nuc="";
						dotB="";


						if(name.equals(firstName)){//se la nuova sequenza e' una subopt di quella sopra
							subopt = true;
						}else{
							subopt = false;
						}

					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}

				}
			}

			//quando esci segnati l'ULTIMA sequenza del file
			if(subopt){
				inputSequences.get(inputSequences.size()-1).getStructureList().add(seq);
			}else{
				inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
				inputSequences.get(inputSequences.size()-1).addSuboptMask();

			}
		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



	static void keepRuns() {
		//metodo attivato se c'� la flag -k
		IO.KEEP=true;
	}
	static boolean getKEEP(){
		return IO.KEEP;
	}

	static void writeGaussian(String pathBench, double gaussMean, double gaussVar) {
		Writer writer = null;
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathBench), "utf-8"));


			writer.write("mean\t" +gaussMean);
			writer.write("\n");
			writer.write("var\t" + gaussVar);

		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}

	}

	static void writeBenchmark(String pathBench, MotifHandler mh, boolean escaped, int contatore, int minSteps, int passiTot, boolean CIRC, int maxWidth){
		//write Benchmark dato il nome del file, l mh, e i parametri di uscita (sostituire anche in run)
		Writer writer = null;
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathBench), "utf-8"));

			String header="BEAR\tID$subopt\tlength\tstart\tend\tscore\n";
					writer.write(header);
			if(CIRC){
				writer.write(mh.toStringCirc(maxWidth));
				writer.write("\n#otherMatches\n");
				writer.write(mh.otherMatchesToStringCirc(maxWidth));

				writer.write("\n#Seq PSSM\n");
				writer.write(mh.toStringSeqCirc(maxWidth));			

				writer.write("\n#DB\n");
				writer.write(mh.toStringDBCirc(maxWidth));	
			}else{
				writer.write(mh.toString());
				writer.write("\n#otherMatches\n");
				writer.write(mh.otherMatchesToString());
	
				writer.write("\n#Seq PSSM\n");
				writer.write(mh.toStringSeq());			
	
				writer.write("\n#DB\n");
				writer.write(mh.toStringDB());	
			}
			writer.write("\n#PSSM\n");
			writer.write(mh.printPSSM());
			writer.write("\n#score\t" + mh.getScore());
			writer.write("\n#seq\t" + mh.cardinality());
			writer.write("\n#width\t" + mh.getMotifWidth());
			writer.write("\n#escape\t" + escaped);
			writer.write("\n#onStep\t" + contatore);
			writer.write("\n#minSteps\t" + minSteps);
			writer.write("\n#maxSteps\t" + passiTot);


		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}

	}

	static void writeBenchmark(String pathBench, MotifHandler mh, boolean escaped, int contatore, int minSteps, int passiTot,
			String method, boolean CIRC, int maxWidth) {
		// for sequences method

		Writer writer = null;
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathBench), "utf-8"));

			if(CIRC){
				writer.write(mh.toStringSeqCirc(maxWidth));
				writer.write("\n#otherMatches\n");
				writer.write(mh.otherMatchesToStringSeqCirc(maxWidth));

				writer.write("\n#Seq PSSM\n");
				writer.write(mh.toStringCirc(maxWidth));			

				writer.write("\n#DB\n");
				writer.write(mh.toStringDBCirc(maxWidth));	
			}else{
			writer.write(mh.toStringSeq());
			writer.write("\n#otherMatches\n");
			writer.write(mh.otherMatchesToStringSeq());
			
			writer.write("\n#Struct PSSM\n");
			writer.write(mh.toString());

			writer.write("\n#DB\n");
			writer.write(mh.toStringDB());
			}


			writer.write("\n#PSSM\n");
			writer.write(mh.printPSSM());


			writer.write("\n#score\t" + mh.getScore());
			writer.write("\n#seq\t" + mh.cardinality());
			writer.write("\n#width\t" + mh.getMotifWidth());
			writer.write("\n#escape\t" + escaped);
			writer.write("\n#onStep\t" + contatore);
			writer.write("\n#minSteps\t" + minSteps);
			writer.write("\n#maxSteps\t" + passiTot);


		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}

	}

	static void writeFailedSearchBenchmark(String pathBench) {
		//write null Benchmark after search if no Negatived were put in
		//In realt� una volta che ci si aggiunge l'input al bg non c'� necessit� di sta cosa
		//perch� ci sar� sempre almeno una sequenza (quelle che compongono il mh)
		Writer writer = null;
		String MESSAGE = "No sequences were found. This means problems. Contact the coders @ marco.pietrosanto at uniroma2.it";
		try {

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathBench), "utf-8"));


			writer.write(MESSAGE);

		} catch (IOException ex) {
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}

	}

	static SearchObj readSearchOutput(String searchFilePath,int totalInput,int totalBackground, String method, KS ks) {
		//legge formato .search.txt, ne ricava dati utilizzati da

		FileReader f = null;
		String s="";
		//farsi tutta la PSSM mi pare esagerato, va bene una matrice di PSSMcell
		ArrayList<ArrayList<PSSMCell>> PSSMdata = new ArrayList<ArrayList<PSSMCell>>();
		//inizializzo dati per l'mcc
		int TP,FP,TN,FN;
		TP=FP=TN=FN=0;
		ArrayList<Double> foreground_partials= new ArrayList<Double>();
		ArrayList<Double> best_fore_partials= new ArrayList<Double>();
		double mediaPart=0.0;
		try{
			//apro file
			f = new FileReader(searchFilePath);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			//riconoscere flag messa al bg
			boolean alignmentZone=true;
			boolean othMzone=false;
			boolean PSSMzone=false;
			s=reader.readLine();
			while ((s=reader.readLine())!=null){
				if (s.trim().equals("")){
					//questa e' generale
					PSSMzone=false;
					othMzone=false;
					alignmentZone=false;
				}
				//le operazioni vanno prima delle flag, altrimenti leggo anche #PSSM etc.
				if (alignmentZone){
					//segna se � TP o FP
					if (s.split("\t")[1].split("[$]")[0].endsWith("_bg")){
						FP++;
					}else{
						TP++;
						//calcolo media parziali
						double part= Double.parseDouble(s.split("\t")[5]);
						mediaPart += part;
						foreground_partials.add(part); //for KS test
						best_fore_partials.add(part);
					}
				}

				if (othMzone){
					if (!s.split("\t")[1].split("[$]")[0].endsWith("_bg")){
						double part= Double.parseDouble(s.split("\t")[5]);
						foreground_partials.add(part); //for pvalue(Nardi)
					}
				}

				if (PSSMzone){
					//riempi PSSMdata
					ArrayList<PSSMCell> tmpAL = new ArrayList<PSSMCell>();
					for (String chunk: s.trim().split("\t")){
						PSSMCell c = new PSSMCell(chunk.charAt(0), Double.parseDouble(chunk.split(":")[chunk.split(":").length-1]));
						tmpAL.add(c);
					}
					PSSMdata.add(tmpAL);
				}

				//flags in mezzo al ciclo
				if (!alignmentZone && s.startsWith("#otherMatches")){
					othMzone=true;
				}
				if (!alignmentZone && s.startsWith("#PSSM")){
					PSSMzone=true;
				}

				//fine flags

			}
			FN = totalInput - TP;
			TN = totalBackground - FP;
			mediaPart /= TP;
			ks.setForeground_partials(foreground_partials); //KS test
			ks.setBestFore_partials(best_fore_partials); //Calculating U -> AUC from best set to assess classification power

		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SearchObj searchObj = new SearchObj(PSSMdata, TP, FP, TN, FN, mediaPart);
		return searchObj;
	}
	
	


	static void appendToSummary(String summaryName, SearchObj sObj, SummarizedData sumData, long elapsedTime, String method){
		DecimalFormat df = new DecimalFormat("0.00");
		DecimalFormat df_ext = new DecimalFormat("0.0000");
		DecimalFormat dfe = new DecimalFormat("0.00E0"); 
		try {
			FileWriter writer = new FileWriter(summaryName, true);
			if (method=="structure"){
				if(!INIT_SUMMARY){
					writer.write("#input=" + sumData.getInputFile());
					writer.write("\n");
					writer.write("#input_size=" + sumData.getInputSize());
					writer.write("\n");
					writer.write("#background=" + sumData.getBgFile());
					writer.write("\n");
					writer.write("#background_size=" + sumData.getBgSize());
					writer.write("\n");
					writer.write("#meanLength=" + df.format(sumData.getMl()));
					writer.write("\n");
					writer.write("#meanStructureContent=" + df.format(sumData.getMsc()));
					writer.write("\n");
					writer.write("#bin=" + sumData.computeBin());
					writer.write("\n");
					INIT_SUMMARY=true;
				}
			}
			if (method=="sequence"){
				if(!INIT_SUMMARY_NUC){
					writer.write("#input=" + sumData.getInputFile());
					writer.write("\n");
					writer.write("#input_size=" + sumData.getInputSize());
					writer.write("\n");
					writer.write("#background=" + sumData.getBgFile());
					writer.write("\n");
					writer.write("#background_size=" + sumData.getBgSize());
					writer.write("\n");
					writer.write("#meanLength=" + df.format(sumData.getMl()));
					writer.write("\n");
					writer.write("#meanStructureContent=" + df.format(sumData.getMsc()));
					writer.write("\n");
					writer.write("#bin=" + sumData.computeBin());
					writer.write("\n");
					INIT_SUMMARY_NUC=true;
				}
			}
			writer.write("\n");
			writer.write("#motif=" + sumData.getMask());
			writer.write("\n");
			writer.write(sumData.getConsensus());
			if(method=="structure"){
				writer.write("#qBEAR\n");
				writer.write(sumData.getqBEARConsensus());
			}

			//			writer.write("MCC=" + sumData.getMCC());
			//			writer.write("\n");
			writer.write("pvalueMW=" + dfe.format(sObj.getPvalueMW()));
			writer.write("\n");	
			writer.write("U=" + df.format(sObj.getStatisticMW()));
			writer.write("\n");	
			writer.write("AUC=" + df.format(sObj.getAUCMW()));
			writer.write("\n");	
			writer.write("pvalueT=" + dfe.format(sObj.getPvalueT()));
			writer.write("\n");	
			writer.write("T=" + df.format(sObj.getStatisticT()));
			writer.write("\n");				
			writer.write("cohend=" + df_ext.format(sObj.getCohen()));
			writer.write("\n");	
			writer.write("cohenU3=" + df_ext.format(sObj.getCohenu3()));
			writer.write("\n");	
			writer.write("pvalueHG=" + dfe.format(sObj.getPvalueHG()));
			writer.write("\n");
			writer.write("score=" + df.format(sumData.getScore()));
			writer.write("\n");
			writer.write("coverage=" + df.format(sumData.getCoverage()));
			writer.write("\n");
			writer.write("fall-out=" + df.format(sumData.getFallout()));
			writer.write("\n");
			writer.write("runtime(s)="+ (((int)elapsedTime/10)/100.0));
			writer.write("\n");




			writer.close();
		} catch (IOException ex) {
			System.err.println("IOException: " + ex.getMessage());
		}
	}

	static void appendToBenchmark(String pathBench, int mask, int bestRunIdx) {
		//TODO append pvalues to benchmark?
		try {
			FileWriter writer = new FileWriter(pathBench, true);
			writer.write("");
		


			writer.close();
		} catch (IOException ex) {
			System.err.println("IOException: " + ex.getMessage());
		}
	}

	static String defaultBgCreation(String bin, int size, String outFolder) {
		//Crea file di background pescando da bin, max 250 (come da RFtest) 
		//restituisce nome del file creato, da passare a SearchAfterRun
		if(bin.startsWith("4")){
			bin="LONG";
			size =190;
			//maximum allowed on LONG
		}

		String BGSRC="/defaultBg/";
		String bgFile=BGSRC+ "noise_rfam_list" + bin + ".fa";
		InputStream stream = IO.class.getResourceAsStream(bgFile);

		//file di output
		String outBgFileName=outFolder + "/autoBG.fa";
		//arrayList in cui saranno storati gli rna del bin
		ArrayList<Motif> inputSequences = new ArrayList<Motif>();

		if (size>250) size=250; //massima eterogeneit� consentita da Rfam

		FileReader f = null;
		/*		try{
			//apro file
			//f = new FileReader(bgFile);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}
		 */
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String s,name="",seq="", firstName="", nuc="", dotB="";
			boolean start=true;

			while ((s=reader.readLine())!=null){

				//System.out.println(s);
				if(start){
					//se non trova che all'inizio c'e' un > allora problema
					if((s.substring(0,1)).equals(">")){
						name=s.substring(1).split("\\$")[0].replaceAll("[\r\n]","");
						start=false;
						//						if(firstSequence){ //se e' la prima subopt segnati il nome
						//							firstName = s.substring(1).split("�")[0];
						//							firstSequence = false;
						//						}

					}else{
						System.out.println("Check file format !\ns.substring(0,1) not equal '>' !!\n" + s.substring(0,1));
						System.exit(-1);
					}

				}else{ //non stiamo all'inizio
					String firstChar=s.substring(0,1);
					if(firstChar.equals(">")){ //se e' un intestazione fasta -> aggiungi la precedente
						//						System.err.println(nuc + "\n" + seq + "\n" + dotB);


						//aggiungi la subopt -0- a inputSequences
						inputSequences.add(new Motif(name, nuc, dotB, seq,0,0));// (aggiunge quella precedente!!)
						firstName = name;
						inputSequences.get(inputSequences.size()-1).addSuboptMask();






						name = s.substring(1).split("\\$")[0].replaceAll("[\r\n]", "");
						seq="";
						nuc="";
						dotB="";




					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[acgurymkswbdhvntACGURYMKSWBDHVNT]*$")){ 
						//controllo per presenza di riga di sequenza
						nuc += s.trim().replaceAll("[\r\n]", "");
					}else if(s.replaceAll("[\r\n]", "").trim().matches("^[.()]*$")){
						//controllo per presenza riga dotBracket
						dotB+= ((s.trim()).replaceAll("[\r\n]",""));
					}else{
						seq+= ((s.trim()).replaceAll("[\r\n]",""));
					}

				}
			}

			//quando esci segnati l'ULTIMA sequenza del file

			inputSequences.add(new Motif(name, nuc,dotB, seq,0,0));
			inputSequences.get(inputSequences.size()-1).addSuboptMask();


		}catch(IOException ioException){ioException.printStackTrace();}

		/*
		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 */

		//A questo punto inputSequences contiene tutto il Bg, 
		//ma vanno selezionati i motif presi a random

		ArrayList<Motif> bgSubset = new ArrayList<Motif>();

		generateRandomSubset(inputSequences, bgSubset, size);

		//scrivo il file finale in outBgFileName

		try {
			FileWriter writer = new FileWriter(outBgFileName, true);

			for(Motif m: bgSubset){
				writer.write(">" + m.getName());
				writer.write("\n");
				writer.write(m.getNucleotides());
				writer.write("\n");
				writer.write(m.getDotBracket());
				writer.write("\n");
				writer.write(m.getStructure());
				writer.write("\n");
			}

			writer.close();
		} catch (IOException ex) {
			System.err.println("IOException: " + ex.getMessage());
		}

		return outBgFileName;
	}

	private static void generateRandomSubset(ArrayList<Motif> inputSequences,
			ArrayList<Motif> bgSubset, int size) {
		//Pesca un numero di sequenze pari a size, rispettando le diverse famiglie
		//(...baciamo le mani)

		Collections.shuffle(inputSequences, new Random(System.currentTimeMillis()));
		for(int i=0;i<size;i++){
			bgSubset.add(inputSequences.get(i));
		}

	}

	static void readInputIDs(String inputFilePath, ArrayList<String> inputSequences) {
		FileReader f = null;
		try{
			//apro file
			f = new FileReader(inputFilePath);
		}catch (IOException ioException){
			ioException.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(f);
			String s,name="",seq="", firstName="", nuc="", dotB="";


			while ((s=reader.readLine())!=null){


				if((s.substring(0,1)).equals(">")){
					name=s.substring(1).replaceAll("[\r\n]","");
					inputSequences.add(name);
				}
			}


		}catch(IOException ioException){ioException.printStackTrace();}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	  /**
     * Export a resource embedded into a Jar file to the local file path.
     *
     * @param resourceName ie.: "/SmartLibrary.dll"
     * @return The path to the exported resource
     * @throws Exception
     */
    static String ExportResource(String resourceName) throws Exception {
        InputStream stream = null;
        OutputStream resStreamOut = null;
        String jarFolder;
        try {
            stream = BEAM2.class.getResourceAsStream(resourceName);//note that each / is a directory down in the "jar tree" been the jar the root of the tree
            if(stream == null) {
                throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
            }

            int readBytes;
            byte[] buffer = new byte[4096];
            jarFolder = new File(BEAM2.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath().replace('\\', '/');
            String currentPath = new File(".").getCanonicalPath();
            //resStreamOut = new FileOutputStream(jarFolder + resourceName);
            resStreamOut = new FileOutputStream(currentPath + resourceName);
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            stream.close();
            resStreamOut.close();
        }

        return jarFolder + resourceName;
    }

	static void writeWebLogo(String webLogoOut, MotifHandler searchMh, String method) {
		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(

					new FileOutputStream(webLogoOut), "utf-8"));

			if(method=="sequence"){
				writer.write(searchMh.toStringSeqWebLogo(true));
			}else{
				writer.write(searchMh.toStringWebLogo(true, true));
			}
		} catch (IOException ex) {
			IO.ERR("could not weblogo");
		} finally {
			try{writer.close();} catch (Exception ex) {}
		}		
	}

}
