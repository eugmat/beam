package BEAM2;

import java.util.ArrayList;

class Motif{
	private String name;
	private String nucleotides;
	private String dotBracket;
	private String structure;
	private ArrayList<String> structure_;
	private int structureIndex;
	private int structureIndexPrev=0; //tiene traccia della precedente subopt (serve per l'undo)
	private int motifStart;
	private int motifStartPrev=0; //tiene traccia del precedente start (serve per l'undo)
	private int motifEnd;
	private int motifEndPrev=0; //tiene traccia del precedente end (serve per l'undo)
	private double partial;
	private double partialPrev=0;
	private ArrayList<ArrayList<int[]>> mask_; //per ogni subopt ci sar� assegnata una serie di maschere, ovvero coppie di posizioni da evitare
	
	
	public Motif(){
		this.structure_ = new ArrayList<String>();
		this.structure_.add("");
		this.mask_ = new ArrayList<ArrayList<int[]>>();
		this.setName("");
		this.setNucleotides("");
		this.setDotBracket("");
		this.setStructure("");
		this.setMotifStart(0);
		this.setMotifEnd(0);
		this.setIndex(0);
		this.setPartial(0.0);
	}

	public Motif(Motif copy){
		this.setName(copy.name);
		this.setNucleotides(copy.nucleotides);
		this.setDotBracket(copy.dotBracket);
		this.mask_ = copy.getMaskList(); //quando si copia un motivo serve anche la maschera

		this.setStructure(copy.structure);
		this.structure_ = new ArrayList<String>(copy.getStructureList());
		this.setIndex(copy.structureIndex);
		this.setMotifStart(copy.motifStart);
		this.setMotifEnd(copy.motifEnd);
		this.setPartial(copy.partial);
	}

	public Motif(String n,String nuc, String seq,int s,int e){
		this.mask_ = new ArrayList<ArrayList<int[]>>();

		this.setName(n);
		this.setNucleotides(nuc);
		this.setDotBracket("");
		this.structure_ = new ArrayList<String>();
		this.structure_.add(seq);
		this.setMotifStart(s);
		this.setMotifEnd(e);
		this.setIndex(0);
		this.setPartial(0.0);

	}
	
	public Motif(String n,String nuc, String dotB, String seq,int s,int e){
		this.mask_ = new ArrayList<ArrayList<int[]>>();

		this.setName(n);
		this.setNucleotides(nuc);
		this.setDotBracket(dotB);
		this.structure_ = new ArrayList<String>();
		this.structure_.add(seq);
		this.setMotifStart(s);
		this.setMotifEnd(e);
		this.setIndex(0);
		this.setPartial(0.0);

	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	
	public String getNucleotides() {
		return nucleotides;
	}
	
	public String getNucleotidesMotif(){
		return nucleotides.substring(motifStart, motifEnd);
	}


	public void setNucleotides(String nucleotides) {
		this.nucleotides = nucleotides;
	}
	
	public String getDotBracket() {
		return dotBracket;
	}

	public void setDotBracket(String dotBracket) {
		this.dotBracket = dotBracket;
	}
	
	public void setStructure(String structure) {
		this.structure = structure;
		
		//this.mask_.add(new ArrayList<int[]>());

	}

	void setStructure(String structure, int index) {
		//setta la sequenza all'indice index dell'arrayList
		this.structure_.set(index, structure);
		
		this.mask_.add(new ArrayList<int[]>());
	}



	public String getStructure(){
		//ritorna l'attuale subopt scelta
		return structure_.get(structureIndex);
	}

	String getStructure(int subopt) {
		//versione a piu' sequenze per gestire piu strutture suboptimali
		return structure_.get(subopt);
	}

	public ArrayList<String> getStructureList(){
		return structure_;
	}
	
	public void setIndex(int index){
		//setta l'index della subopt scelta
		this.structureIndexPrev = this.structureIndex;
		this.structureIndex = index;
		//IO.INFO("in caso di ripristino: " + this.sequenceIndexPrev + "\t attuale: "+ this.sequenceIndex);
	}
	public int getIndex(){
		return this.structureIndex;
	}
	
	public int getIndexPrev(){
		return this.structureIndexPrev;
	}

	public void clearStartEndIndexes(){
		this.motifEnd=0;
		this.motifEndPrev=0;
		this.motifStart=0;
		this.motifStartPrev=0;
	}

	private int getMotifWidth(){
		return this.motifEnd - this.motifStart;
	}
	
	boolean setMotifStart(int motifStart) {
		///*
		if(isMasked(motifStart) || isMasked(motifStart + this.getMotifWidth())){ //ancora incerto per il resize (MA dovrebbe essere gestito a monte nel metodo)
			return false; //annuncio che ho provato a settare uno start non fattibile
		}//*/
		if(motifStart >= 0){
			this.motifStartPrev=this.motifStart;
			this.motifStart = motifStart;
		} else {
			IO.ERR("Index Start minore di 0 !");
			System.exit(-1);
		}
		
		return true;
		
	}
	
	boolean setMotifStart(int motifStart, int motifWidth){
		///*
		if(isMasked(motifStart) || isMasked(motifStart + motifWidth)){ //ancora incerto per il resize (MA dovrebbe essere gestito a monte nel metodo)
			this.motifStartPrev=this.motifStart;
			return false; //annuncio che ho provato a settare uno start non fattibile
		}//*/
		if(motifStart >= 0){
			this.motifStartPrev=this.motifStart;
			this.motifStart = motifStart;
		} else {
			IO.ERR("Index Start minore di 0 !");
			return false;
		}
		
		return true;
		
	}

	public int getMotifStart() {
		return motifStart;
	}


	boolean setMotifEnd(int motifEnd) {
		///*
		if(isMasked(motifEnd) || isMasked(motifEnd - this.getMotifWidth() )  ){
			//IO.ERR("masked");
			return false; //annuncio che ho provato a settare un end non fattibile
		}//*/
		if(motifEnd <= this.getStructure().length()){
			this.motifEndPrev=this.motifEnd;
			this.motifEnd = motifEnd;
		}else if(motifEnd <= 0){
			IO.ERR(this.name + "\t" + motifEnd);
			IO.ERR("Index End <= 0 !");
			System.exit(-1);
		}else{
			IO.ERR(this.name + "\t" + this.structure_.get(structureIndex).length() + "\t" + motifEnd);
			IO.ERR("Index End greater than max length !");
			IO.ERR("Index sub: " + this.structureIndex);

			System.exit(-1);
		}
		
		return true;
		
	}
	boolean setMotifEnd(int motifEnd,int motifWidth) {
		///*
		if(isMasked(motifEnd) || isMasked(motifEnd - motifWidth )  ){
			this.motifEndPrev=this.motifEnd;
			//IO.ERR("masked");
			this.setMotifStartUndo();
			return false; //annuncio che ho provato a settare un end non fattibile
		}//*/
		if(motifEnd <= this.getStructure().length()){
			this.motifEndPrev=this.motifEnd;
			this.motifEnd = motifEnd;
		}else if(motifEnd <= 0){
			IO.ERR(this.name + "\t" + motifEnd);
			IO.ERR("Index End <= 0 !");
			System.exit(-1);
		}else{
			IO.ERR(this.name + "\t" + this.structure_.get(structureIndex).length() + "\t" + motifEnd);
			IO.ERR("Index End greater than max length !");
			IO.ERR("Index sub: " + this.structureIndex);

			System.exit(-1);
		}
		
		return true;
		
	}
	
	
	public int getMotifEnd() {
		return motifEnd;
	}

	public void setPartial(double partial_) {
		this.partialPrev = this.partial;
		this.partial = partial_;
	}	
	
	public double getPartial() {
		return this.partial;
	}

	
	void setMotifStartUndo() {
		this.motifStart=this.motifStartPrev;
	}

	void setMotifEndUndo() {
		this.motifEnd = this.motifEndPrev;
	}

	void setIndexUndo() {
		this.structureIndex = this.structureIndexPrev;
	}

	void setPartialUndo() {
		this.partial = this.partialPrev;
	}	
	
	String extractMotifFromStructure() {
		return this.getStructure().substring(motifStart, motifEnd);
	}

	String extractMotifFromNucleotides() {
		return this.getNucleotides().substring(motifStart, motifEnd);
	}
	
	String extractMotifFromDB() {
		return this.getDotBracket().substring(motifStart, motifEnd);
	}

	//MAIN method
	void addSuboptMask(){
		this.mask_.add(new ArrayList<int[]>());		
	}
	
	void addMask(int start, int end){
		int[] se = new int[2];
		se[0] = start;
		se[1] = end;
		//this.mask_.get(sequenceIndex).add(se);
		this.mask_.get(0).add(se);

	}
	
	public ArrayList<ArrayList<int[]>> getMaskList(){
		return this.mask_;
	}
	
	public ArrayList<int[]> getMask(){
		return this.mask_.get(this.structureIndex);
	}
	
	void setMask(int subIndex, int maskIndex, int[] newMask){
		this.getMaskList().get(subIndex).set(maskIndex, newMask);
	}
	void removeMask(int subIndex, int maskIndex){
		this.getMaskList().get(subIndex).remove(maskIndex);
	}
	
	String printMask(){
		String s = name;
		for(ArrayList<int[]> al: mask_){
			//s += "\nsub:\n";
			for(int[] msk: al){
				s += "\t[" + msk[0] + "," + msk[1]+ "]\t";
			}
		}
		
		return s;
	}
	
	@Override
	public String toString() {
		String motifString = "";
		motifString=">"+this.name + "|" + this.motifStart + "|" + this.motifEnd + "\n" + this.getStructure() + "\n";
		return motifString;
	}

	public boolean equals(Motif m){
		// if the two objects are equal in reference, they are equal
		if (this == m) {
			return true;
		} else if (m instanceof Motif) {
			if (m.getName().equals(this.getName()) && m.getStructure().equals(this.getStructure())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	boolean isMasked(int position){
		//if position is in any of the choosen subopt masks then return true.
		if(mask_.isEmpty()){ //non fare il controllo se la maschera non esiste
			return false;
		}
		boolean isMasked_= false;
				for(int[] se: mask_.get(0)){
					if(position >= se[0] && position < se[1]){
						isMasked_=true;
						return isMasked_;
					}
				}
			return isMasked_;
	}

	public int getMotifEndPrev() {
		// TODO Auto-generated method stub
		return motifEndPrev;
	}

	public int getMotifStartPrev() {
		// TODO Auto-generated method stub
		return motifStartPrev;
	}



}
