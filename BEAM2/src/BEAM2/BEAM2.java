package BEAM2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
//MULTI RUN BRANCH
//___
import java.util.ArrayList;

class BEAM2 {
	private static boolean acceptScore(double scoreVecchio, double scoreNuovo, double temperature) {

		double prob = Math.exp(1*(scoreNuovo - scoreVecchio)/temperature);
		if (temperature == 0){
			prob = 0;
		}
		if(Math.random() < prob ){
			return true;
		}
		else{
			return false;
		}
	}
	private static boolean acceptScoreSeq(double scoreVecchio, double scoreNuovo, double temperature) {
		//Score deltas are approximately 15 times higher than structure's
		double prob = Math.exp(0.06666666667*(scoreNuovo - scoreVecchio)/temperature);
		
		if (temperature == 0){
			prob = 0;
		}
		if(Math.random() < prob ){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * @param args
	 */
	public static MotifMemory run(String[] args, int idx, int mask, String output, ArrayList<Motif> inputSequences) {
		long startTime = System.currentTimeMillis();

		//ArrayList<Motif> inputSequences=new ArrayList<Motif>();

		MotifUtilities.reshapeInput(inputSequences);
		
		int escape = 0;

		double[][]mbr=SubsMat.getmbr();	

		MotifManager manager=new MotifManager();

		//default
		int motifWidth=10;
		int startingNoOfSequences = 3;
		int model_limit = 100;

		double temperatureStart=100;
		double coolingRate=0.001;//These two variables can be fixed values or can be taken as input
		double minTemp = 0.001;


		int widthUpperBound = 100;

		int minSteps = 10000;
		double passiTot=15000; 

		int escapeCondition = 1;

		int bestMW = 0;

		final int N = 5; //numero medio di operazioni per sequenza:controlla il numero minimo di passi
		//CommandLine Parser 
		//TODO pure tutti questi comandi sarebbe meglio che li leggesse una volta sola
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)

		//do not touch
		double maximumScoreEver=-9999;
		boolean escaped = false;
		double temperature = temperatureStart;
		boolean clean = true; //parziali > 50% media
		boolean superClean = true; //parziali >0
		boolean ultraClean = true; //parziali >0

		boolean branching = true;
		boolean weHaveSubopt = false;
		boolean CIRC = false;
		
		//-----LETTURA COMMAND LINE ARGUMENTS (CLA)
		if(clp.containsKey("s")) minSteps=Integer.parseInt(clp.getValue("s")); //numero min di passi prima di fermarsi
		if(clp.containsKey("W")) widthUpperBound=Integer.parseInt(clp.getValue("W")); //limite superiore larghezza motif
		if(clp.containsKey("w")) motifWidth=Integer.parseInt(clp.getValue("w")); //starting motif width
		if(clp.containsKey("T")) temperature=Double.parseDouble(clp.getValue("T")); //starting T
		if(clp.containsKey("r")) coolingRate=Double.parseDouble(clp.getValue("r")); //cooling rate, precision

		if(clp.containsKey("n")) model_limit=Integer.parseInt(clp.getValue("n")); //sampling iniziale
		if(clp.containsKey("C") && clp.getValue("C").equals("1")) {clean=true;superClean=false;ultraClean=false;}
		else if(clp.containsKey("C") && clp.getValue("C").equals("2")){clean=true;superClean=true;ultraClean=false;}
		else if(clp.containsKey("C") && clp.getValue("C").equals("3")){clean=true;ultraClean=true;}
		if(clp.containsKey("b")) {
			if (clp.getValue("b").equals("F")) branching=false;
		}
		if(clp.containsKey("m") && clp.getValue("m").equals("T")) weHaveSubopt = true;
		if(clp.containsKey("circ")){
			CIRC = true;
		}
		//----------
		//		percentage =startingNoOfSequences/(double)inputSequences.size();
		
		if (!branching){
			MotifUtilities.setBranchingZero(mbr);
			if (Debug.VERBOSE){
				IO.INFO("Setting :-: equal to 0...mbr[:][:]: " + BEARManager.substitutionScore(':', ':', mbr));
			}
		} else {
			if (Debug.VERBOSE){
				IO.INFO("branch to branch substitution considered: " + BEARManager.substitutionScore(':', ':', mbr));
			}
		}


		coolingRate = (temperatureStart - minTemp)/minSteps; //in pratica quando la T va a 0.0


		//-----QUI FINISCONO I PARAMETRI DI INPUT-----

		//		IO.INFO(motifWidth + " " + inputSequences.size() + " " + percentage);
		MotifHandler mh = null;
		MotifHandler bestMh = new MotifHandler();
		int mW=0;

		if ( mask == 1 && idx == 1){
			//IO.INFO(inputSequences.size());
			mh=manager.initialise(motifWidth,inputSequences,mbr, startingNoOfSequences);//genera la prima soluzione casuale		
		}else{
			mh=manager.initialise2(motifWidth,inputSequences,mbr,startingNoOfSequences);
		}
		if (mh == null) return null;
		
		if (Debug.VERBOSE) {IO.INFO("---GENERATED STARTING MH---\n"+mh.toString());}
		//---------------------
		PSSM.computePSSM(mh, inputSequences);
		mh.setMotifWidth();		

		if (Debug.VERBOSE){
			if (mh.getObjectMotif(0).getNucleotides().equals("")){
				IO.ERR("Nucleotide sequences not found");
			}
		}
		
		if (Debug.ON) Debug.checkRNA(mh);
		MotifUtilities.computePartials(mh.getPSSM(), mh, mbr, mh.getMotifWidth());
		MotifHandler.computeScore(mh);//genera primo punteggio
		double prevScore = mh.getScore();
		int contatore=0;

		//per avere almeno N operazioni su ogni sequenza in media (7 operazioni, X subopt)
		//CLA
		if(clp.containsKey("S")) {
			passiTot=Double.parseDouble(clp.getValue("S")); //force passiTot
		}else{


			//-----------
			passiTot = inputSequences.size()*7*mh.getObjectMotif(0).getStructureList().size()*N; 		

			if (passiTot < minSteps){
				passiTot = minSteps + inputSequences.size()*7*mh.getObjectMotif(0).getStructureList().size();
			}			
			if (passiTot > 15000){
				passiTot=15000;
			}
		}
		

		escapeCondition = Math.max((int)(passiTot*.005),1000); //anche in un dataset piccolo richiediamo almeno 1000 steps per scappare



		//se dopo aver in media agito con ogni perturbazione su tutte le sequenze una volta non cambia nulla, allora escape, ma solo se sono stati fatti almeno minSteps

		if (Debug.VERBOSE) {
			IO.INFO("..............STARTING MOTIF..............\n");
			IO.INFO(mh.toString());
			IO.INFO("Score : " + mh.getScore());
			IO.INFO("Length : " + mh.getMotifWidth());
			IO.INFO("#Sequences : " + mh.cardinality() +"\n");
		}

		/*---------*/
		double[][] media;
		media = new double[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] count;
		count = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] acceptSecondCount;
		int[][] rejectCount;
		acceptSecondCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		rejectCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};

		/*---------*/





		//

		double currentScore=mh.getScore();
		
		//-----INIZIO CICLO -----

		while((escape < escapeCondition || contatore < minSteps) && contatore < passiTot ){
			//se anche e' stata raggiunta la condizione di escape non fermarti prima di minSteps, in ogni caso esci dopo passiTot
			if (Debug.VERBOSE) {IO.INFO("\n>>>>>>>>>>>>>>>ITERATION : "+contatore+ " di " + passiTot + "<<<<<<<<<<<<<<<");}
			currentScore=mh.getScore();
			//IO.INFO(perturbatedMotif.motifLength + "\t" + m.motifLength);

			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}else{
				//				IO.ERR(mh.cardinality());
			}

			//-----PERTURBO IL MOTIVO, TENGO LA SOLUZIONE PRECEDENTE-----
			int operation = manager.perturbateMotif(mh,inputSequences,mbr, widthUpperBound, weHaveSubopt, model_limit);

			//-----NEL CASO NORMALE CALCOLA PSSM E SCORE-----
			if(mh.cardinality()<2){
				//-----SE IL MH CONTIENE 1 SEQUENZE INVECE FAI UN ADD-----		
				currentScore=0;
				mh.setMotifWidth(motifWidth);
				operation = manager.perturbateMotif(mh,inputSequences,mbr, widthUpperBound, weHaveSubopt, model_limit);
			}
			if (operation == -1){
				break;
			}
			//SE CI SONO PROBLEMI DECOMMENTARE QUESTO SETMOTIFWIDTH ALERT ALERT ALERT!
			//mh.setMotifWidth();

			if (Debug.VERBOSE){
				IO.INFO(mh.toString());
			}
			PSSM.computePSSM(mh, inputSequences);
			MotifUtilities.computePartials(mh.getPSSM(), mh, mbr, mh.getMotifWidth());
			MotifHandler.computeScore(mh);

			/*Test per peso singole operazioni*/
			double delta = mh.getScore() - currentScore;

			if (contatore < minSteps){
				media[(int)(contatore/(minSteps/3.0))][operation] += delta;
				count[(int)(contatore/(minSteps/3.0))][operation] += 1;
			}
			/*-------------------------------*/
			double newScore = mh.getScore();

			if(newScore>=currentScore){
				if (Debug.VERBOSE) {IO.INFO("---ACCEPTED---\n");}
				currentScore=newScore;

			}else if(acceptScore(currentScore,newScore, temperature)){
				if (Debug.VERBOSE) {IO.INFO("---ACCEPTED SECOND CHANCE---\n");}				
				currentScore=mh.getScore();

				acceptSecondCount[(int)(contatore/(passiTot/3))][operation] += 1;
			}else{
				if (Debug.VERBOSE) {IO.INFO("\n---REJECTED---\n");}
				MotifManager.ctrlZ(operation, mh, inputSequences);//il ctrlZ non riprende il vecchio score!!! ora si
				mh.getPSSM().setScore(currentScore); //aggiunto : maxScore e' semplicemente lo score del giro precedente

				if (contatore<minSteps-1){
					rejectCount[(int)(contatore/minSteps*3)][operation] += 1;
				}

			}


			if (Debug.VERBOSE) {	IO.INFO("---cooling down---\n");
			}
			if(temperature > minTemp){
				temperature-=coolingRate;
			}else{
				temperature = 0;
			}


			//----------------------------TEST MAX SCORE----------------------------


			if(newScore >= prevScore){
				//se migliora lo score, non aumentare l'escape, anzi azzeralo. A meno che non sia aumentato di troppo poco.
				maximumScoreEver = newScore;

				bestMh.MotifHandlerClone(mh);
				bestMh.getPSSM().setScore(maximumScoreEver); 
				bestMW=mh.getMotifWidth();

				int threshold = 1;
				if(Math.abs(mh.getScore() - prevScore) < threshold){ //TODO da stimare meglio
					escape++;
					//IO.ERR("escape in: " + (escapeCondition - escape) + " passi");
				}else{
					escape = 0;
				}
			}
			prevScore = newScore;
			//----------------------------TEST MAX SCORE END----------------------------


			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}
			
			contatore++;

			if (Debug.VERBOSE) {
				IO.INFO("Temperature : " + temperature);
				IO.INFO("Score : " + mh.getScore());
				IO.INFO("Length : " + mh.getMotifWidth());
				IO.INFO("#Sequences : " + mh.cardinality() +"\n");
			}else if(contatore%(int)(passiTot/10) == 0 ){
				IO.INFO(Math.round(contatore/passiTot*100) + "%");
			}
			mW=bestMh.getMotifWidth();
		}
		IO.INFO("\n-------------END OF COMPUTATION-------------\n");

		bestMh.adjustEndIndexes(bestMW);
		if(escape >= escapeCondition) escaped = true;

		
		if (bestMh.cardinality() == 0){
			bestMh.MotifHandlerClone(mh);
			bestMh.setMotifWidth(mh.getMotifWidth());
			bestMh.adjustEndIndexes(mh.getMotifWidth());
			PSSM.computePSSM(bestMh, inputSequences);
			bestMh.getPSSM().setScore(mh.getScore());
		}else{
			PSSM.computePSSM(bestMh, inputSequences);
		}
		
		if (Debug.VERBOSE){
		
			IO.INFO("BestMh PSSM --------------------");
			IO.INFO(bestMh.printPSSM());

		}


		MotifUtilities.computePartials(bestMh.getPSSM(), bestMh, mbr, bestMh.getMotifWidth());

		//Pulizia --------- (--clean <- non funge? -C? non funge?)
		IO.INFO("clean: " + clean);

		if (clean){
			IO.INFO("\n[-------------CLEANING INITIATED-------------]\n");

			double thr = 0.0;
			if(ultraClean){
				thr = 0.99;
			}else if(superClean){
				thr= .5;
			}
			MotifUtilities.clean(bestMh, thr, inputSequences);

			PSSM.computePSSM(bestMh, inputSequences); //rifaccio dopo il clean
			motifWidth=bestMh.getMotifWidth();
			MotifUtilities.computePartials(bestMh.getPSSM(), bestMh, mbr, motifWidth);

			//IO.INFO("Motif Score : "+mh.getScore());
			IO.INFO("Motif Score : "+bestMh.getScore());

			MotifHandler.computeScore(bestMh);

			//IO.INFO("Motif Score after Cleaning : "+mh.getScore());
			IO.INFO("Motif Score after Cleaning : "+bestMh.getScore());
			IO.INFO("\n[-------------CLEANING COMPLETED-------------]\n");
		}


		if(mh.cardinality() != 0){
			
			mh.setMotifWidth();}
		if(bestMh.cardinality() != 0){
			bestMh.getListMotif().get(0).setMotifEnd(bestMh.getListMotif().get(0).getMotifStart()+bestMW);
			bestMh.setMotifWidth();
		}

		//Media Delta Score
		if (Debug.ON){ 
			IO.INFO("\n-------------Mean Delta Score STRUCTURE-------------\n");
			for(int parte = 0; parte < 3; parte++){
				for(int k = 0; k < media[parte].length; k++){
					media[parte][k] /= count[parte][k];
				}
			}	

			for (int phase = 0; phase<=2; phase++){
				if(phase == 0){
					IO.INFO("\nfase high T-----");
				}else if(phase == 1){
					IO.INFO("\nfase mid T-----");
				}else{
					IO.INFO("\nfase low T-----");
				}
				for (int op = 0; op<=5; op++){
					if (op == 0){
						System.out.print("media add: ");
					}else if(op == 1){
						System.out.print("media remove: ");
					}else if(op == 2){
						System.out.print("media shift: ");
					}else if(op == 3){
						System.out.print("media recalculate: ");
					}else if(op == 4){
						System.out.print("media expand: ");
					}else if(op == 5){
						System.out.print("media shrink: ");
					}
					IO.INFO(media[phase][op] + "\t#operazioni: " + count[phase][op] +
							"\taccept: " + (count[phase][op]-acceptSecondCount[phase][op]-rejectCount[phase][op]) + "\taccept2nd: " + acceptSecondCount[phase][op] + 
							"\treject: " + rejectCount[phase][op]);
				}
			}
		}
		
		IO.INFO("\n----- final local alignment -----\n");


		String fileBench="lastrun_m" + mask + "_run"  + idx ;
		if (clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			fileBench = tmp[tmp.length-1].split("\\.")[0]+ "_m" + mask + "_run"  + idx; //
		}
		Writer writer = null;

		//WebLogo

//		String webLogoOut = output + "/webLogoOut/mask"+ mask +"/" + fileBench + "_wl.fa"; //output da dare a weblogo
//		boolean webLogo=false;
//		if(webLogo){
//			IO.INFO(bestMh.toString(webLogo,true));
//		}	
//
//
//		try {
//			writer = new BufferedWriter(new OutputStreamWriter(
//
//					new FileOutputStream(webLogoOut), "utf-8"));
//
//
//			writer.write(bestMh.toString(webLogo, true));
//
//		} catch (IOException ex) {
//		} finally {
//			try{writer.close();} catch (Exception ex) {}
//		}

		//write Benchmark

		String pathBench = output + "/benchmark/mask" + mask + "/" + fileBench + ".txt";
		
		IO.writeBenchmark(pathBench, bestMh, escaped, contatore, minSteps, (int)passiTot, CIRC, widthUpperBound);
//		try {
//
//			writer = new BufferedWriter(new OutputStreamWriter(
//					new FileOutputStream(pathBench), "utf-8"));
//
//
//			writer.write(bestMh.toString());
//			writer.write("\n#otherMatches\n");
//			writer.write(bestMh.otherMatchesToString());
//
//			if (bestMh.getObjectMotif(0).getNucleotides().equals("") == false){
//				writer.write("\n#Seq PSSM\n");
//				writer.write(bestMh.toStringSeq());			
//			}
//
//
//			writer.write("\n#PSSM\n");
//			writer.write(bestMh.printPSSM());
//			writer.write("\n#score\t" + bestMh.getScore());
//			writer.write("\n#seq\t" + bestMh.cardinality());
//			writer.write("\n#width\t" + bestMh.getMotifWidth());
//
//			writer.write("\n#escape\t" + escaped);
//			writer.write("\n#onStep\t" + contatore);
//			writer.write("\n#minSteps\t" + minSteps);
//			writer.write("\n#maxSteps\t" + passiTot);
//
//
//		} catch (IOException ex) {
//		} finally {
//			try {writer.close();} catch (Exception ex) {}
//		}



		

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		IO.INFO("run time: " + ((int)elapsedTime/10)/100.0 + " s");

		return new MotifMemory(bestMh.getScore(), bestMh);

	}
	
	static MotifMemory runSequence(String[] args, int idx, int mask, String output, ArrayList<Motif> inputSequences) {
		long startTime = System.currentTimeMillis();

		//ArrayList<Motif> inputSequences=new ArrayList<Motif>();
		
		//check for duplicates at the beginning of the run. Cause: unknown (previous methods)
		MotifUtilities.reshapeInput(inputSequences);
		
		int escape = 0;
		
		//Read substitution matrix (nucleotides
		double[][]mat=SubsMat.getNuclSubsMat();	

		MotifManager manager=new MotifManager();

		//default
		int motifWidth=10;
		int startingNoOfSequences = 3;
		int model_limit = 100;

		double temperatureStart=100;
		double coolingRate=0.001;//These two variables can be fixed values or can be taken as input
		double minTemp = 0.001;


		int widthUpperBound = 100;

		int minSteps = 10000;
		double passiTot=15000; 

		int escapeCondition = 1;

		int bestMW = 0;

		final int N = 5; //numero medio di operazioni per sequenza:controlla il numero minimo di passi
		//CommandLine Parser 
		//TODO pure tutti questi comandi sarebbe meglio che li leggesse una volta sola
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)

		//do not touch
		double maximumScoreEver=-9999;
		boolean escaped = false;
		double temperature = temperatureStart;
		boolean clean = true; //parziali > 50% media
		boolean superClean = true; //parziali 
		boolean ultraClean = true; //parziali >.99media

		@SuppressWarnings("unused")
		boolean branching = true;
		boolean weHaveSubopt = false;
		boolean CIRC = false;
		//ONLY FOR SEQUENCES: no upper width limits (and fixed lower). Subs matrix should handle that
		widthUpperBound=99999;
		motifWidth=3; //min
		//
		
		//-----LETTURA COMMAND LINE ARGUMENTS (CLA)
		if(clp.containsKey("s")) minSteps=Integer.parseInt(clp.getValue("s")); //numero min di passi prima di fermarsi
		if(clp.containsKey("T")) temperature=Double.parseDouble(clp.getValue("T")); //starting T
		if(clp.containsKey("r")) coolingRate=Double.parseDouble(clp.getValue("r")); //cooling rate, precision

		if(clp.containsKey("n")) model_limit=Integer.parseInt(clp.getValue("n")); //sampling iniziale
		if(clp.containsKey("C") && clp.getValue("C").equals("1")) {clean=true;superClean=false;ultraClean=false;}
		else if(clp.containsKey("C") && clp.getValue("C").equals("2")){clean=true;superClean=true;ultraClean=false;}
		else if(clp.containsKey("C") && clp.getValue("C").equals("3")){clean=true;ultraClean=true;}
		if(clp.containsKey("b")) {
			if (clp.getValue("b").equals("F")) branching=false;
		}
		if(clp.containsKey("m") && clp.getValue("m").equals("T")) weHaveSubopt = true;
		if(clp.containsKey("circ")){
			CIRC=true;
		}
		//----------
		//		percentage =startingNoOfSequences/(double)inputSequences.size();
		


		coolingRate = (temperatureStart - minTemp)/minSteps; //in pratica quando la T va a 0.0


		//-----QUI FINISCONO I PARAMETRI DI INPUT-----

		//		IO.INFO(motifWidth + " " + inputSequences.size() + " " + percentage);
		MotifHandler mh = null;
		MotifHandler bestMh = new MotifHandler();
		int mW=0;

		if ( mask == 1 && idx == 1){
			//IO.INFO(inputSequences.size());
			mh=manager.initialiseSequence(motifWidth,inputSequences,mat, startingNoOfSequences);//genera la prima soluzione casuale		
		}else{
			mh=manager.initialise2Sequence(motifWidth,inputSequences,mat,startingNoOfSequences);
		}
		if (mh == null) return null;

		if (Debug.VERBOSE) {IO.INFO("---GENERATED STARTING MH---\n"+mh.toString());}
		//---------------------
		PSSM.computePSSM_Nucleotides(mh, inputSequences);
		mh.setMotifWidth();		

		if (Debug.VERBOSE){
			if (mh.getObjectMotif(0).getNucleotides().equals("")){
				IO.ERR("Nucleotide sequences not found");
			}
		}
		
		if (Debug.ON) Debug.checkRNA(mh);
		MotifUtilities.computePartials_Nucleotides(mh.getPSSM(), mh, mat, mh.getMotifWidth());
		MotifHandler.computeScore(mh);//genera primo punteggio
		double prevScore = mh.getScore();
		int contatore=0;

		//per avere almeno N operazioni su ogni sequenza in media (7 operazioni, X subopt)
		//CLA
		if(clp.containsKey("S")) {
			passiTot=Double.parseDouble(clp.getValue("S")); //force passiTot
		}else{


			//-----------
			passiTot = inputSequences.size()*7*mh.getObjectMotif(0).getStructureList().size()*N; 		

			if (passiTot < minSteps){
				passiTot = minSteps + inputSequences.size()*7*mh.getObjectMotif(0).getStructureList().size();
			}			
			if (passiTot > 15000){
				passiTot=15000;
			}
		}
		

		escapeCondition = Math.max((int)(passiTot*.005),1000); //anche in un dataset piccolo richiediamo almeno 1000 steps per scappare



		//se dopo aver in media agito con ogni perturbazione su tutte le sequenze una volta non cambia nulla, allora escape, ma solo se sono stati fatti almeno minSteps

		if (Debug.VERBOSE) {
			IO.INFO("..............STARTING MOTIF..............\n");
			IO.INFO(mh.toStringSeq());
			IO.INFO("Score : " + mh.getScore());
			IO.INFO("Length : " + mh.getMotifWidth());
			IO.INFO("#Sequences : " + mh.cardinality() +"\n");
		}

		/*---------*/
		double[][] media;
		media = new double[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] count;
		count = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		int[][] acceptSecondCount;
		int[][] rejectCount;
		acceptSecondCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
		rejectCount = new int[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};

		/*---------*/





		//

		double currentScore=mh.getScore();
		
		//-----INIZIO CICLO -----

		while((escape < escapeCondition || contatore < minSteps) && contatore < passiTot ){
			//se anche e' stata raggiunta la condizione di escape non fermarti prima di minSteps, in ogni caso esci dopo passiTot
			if (Debug.VERBOSE) {IO.INFO("\n>>>>>>>>>>>>>>>ITERATION : "+contatore+ " di " + passiTot + "<<<<<<<<<<<<<<<");}
			currentScore=mh.getScore();

			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}else{
				//				IO.ERR(mh.cardinality());
			}

			//-----PERTURBO IL MOTIVO, TENGO LA SOLUZIONE PRECEDENTE-----
			int operation = manager.perturbateMotif_Nucleotides(mh,inputSequences,mat, widthUpperBound, weHaveSubopt, model_limit);

			//-----NEL CASO NORMALE CALCOLA PSSM E SCORE-----
			if(mh.cardinality()<2){
				//-----SE IL MH CONTIENE 1 SEQUENZE INVECE FAI UN ADD-----		
				currentScore=0;
				mh.setMotifWidth(motifWidth);
				operation = manager.perturbateMotif_Nucleotides(mh,inputSequences,mat, widthUpperBound, weHaveSubopt, model_limit);
			}
			if (operation == -1){
				break;
			}

			if (Debug.VERBOSE){
				IO.INFO(mh.toStringSeq());
			}
			PSSM.computePSSM_Nucleotides(mh, inputSequences);
			MotifUtilities.computePartials_Nucleotides(mh.getPSSM(), mh, mat, mh.getMotifWidth());
			MotifHandler.computeScore(mh);

			/*Test per peso singole operazioni*/
			double delta = mh.getScore() - currentScore;

			if (contatore < minSteps){
				media[(int)(contatore/(minSteps/3.0))][operation] += delta;
				count[(int)(contatore/(minSteps/3.0))][operation] += 1;
			}
			/*-------------------------------*/
			double newScore = mh.getScore();

			if(newScore>=currentScore){
				if (Debug.VERBOSE) {IO.INFO("---ACCEPTED---\n");}
				currentScore=newScore;

			}else if(acceptScoreSeq(currentScore,newScore, temperature)){
				if (Debug.VERBOSE) {IO.INFO("---ACCEPTED SECOND CHANCE---\n");}				
				currentScore=mh.getScore();

				acceptSecondCount[(int)(contatore/(passiTot/3))][operation] += 1;
			}else{
				if (Debug.VERBOSE) {IO.INFO("\n---REJECTED---\n");}
				MotifManager.ctrlZ(operation, mh, inputSequences);//il ctrlZ non riprende il vecchio score!!! ora si
				mh.getPSSM().setScore(currentScore); //aggiunto : maxScore e' semplicemente lo score del giro precedente

				if (contatore<minSteps-1){
					rejectCount[(int)(contatore/minSteps*3)][operation] += 1;
				}

			}


			if (Debug.VERBOSE) {	IO.INFO("---cooling down---\n");
			}
			if(temperature > minTemp){
				temperature-=coolingRate;
			}else{
				temperature = 0;
			}


			//----------------------------TEST MAX SCORE----------------------------


			if(newScore >= prevScore){
				//se migliora lo score, non aumentare l'escape, anzi azzeralo. A meno che non sia aumentato di troppo poco.
				maximumScoreEver = newScore;

				bestMh.MotifHandlerClone(mh);
				bestMh.getPSSM().setScore(maximumScoreEver); 
				bestMW=mh.getMotifWidth();

				int threshold = 1;
				if(Math.abs(mh.getScore() - prevScore) < threshold){ //TODO da stimare meglio
					escape++;
					//IO.ERR("escape in: " + (escapeCondition - escape) + " passi");
				}else{
					escape = 0;
				}
			}
			prevScore = newScore;
			//----------------------------TEST MAX SCORE END----------------------------


			if(mh.cardinality() >= 1){
				mh.setMotifWidth();
			}
			
			contatore++;

			if (Debug.VERBOSE) {
				IO.INFO("Temperature : " + temperature);
				IO.INFO("Score : " + mh.getScore());
				IO.INFO("Length : " + mh.getMotifWidth());
				IO.INFO("#Sequences : " + mh.cardinality() +"\n");
			}else if(contatore%(int)(passiTot/10) == 0 ){
				IO.INFO(Math.round(contatore/passiTot*100) + "% - Nucleotides");
			}
			mW=bestMh.getMotifWidth();
		}
		IO.INFO("\n-------------END OF COMPUTATION-------------\n");

		bestMh.adjustEndIndexes(mW);
		if(escape >= escapeCondition) escaped = true;

		
		if (bestMh.cardinality() == 0){
			bestMh.MotifHandlerClone(mh);
			bestMh.setMotifWidth(mh.getMotifWidth());
			bestMh.adjustEndIndexes(mh.getMotifWidth());
			PSSM.computePSSM_Nucleotides(bestMh, inputSequences);
			bestMh.getPSSM().setScore(mh.getScore());
		}else{
			PSSM.computePSSM_Nucleotides(bestMh, inputSequences);
		}
		
		if (Debug.VERBOSE){
		
			IO.INFO("BestMh PSSM --------------------");
			IO.INFO(bestMh.printPSSM());

		}


		MotifUtilities.computePartials_Nucleotides(bestMh.getPSSM(), bestMh, mat, bestMh.getMotifWidth());

		//Pulizia --------- (--clean <- non funge? -C? non funge?)
		IO.INFO("clean: " + clean);

		if (clean){
			IO.INFO("\n[-------------CLEANING INITIATED (SEQUENCE)-------------]\n");

			double thr = 0.0;
			if(ultraClean){
				thr = 0.99;
			}else if(superClean){
				thr= .5;
			}

			
			MotifUtilities.clean(bestMh, thr, inputSequences);
			//good


			PSSM.computePSSM_Nucleotides(bestMh, inputSequences); //rifaccio dopo il clean
			motifWidth=bestMh.getMotifWidth();
			MotifUtilities.computePartials_Nucleotides(bestMh.getPSSM(), bestMh, mat, motifWidth);

			//IO.INFO("Motif Score : "+mh.getScore());
			IO.INFO("Motif Score : "+bestMh.getScore());

			MotifHandler.computeScore(bestMh);

			//IO.INFO("Motif Score after Cleaning : "+mh.getScore());
			IO.INFO("Motif Score after Cleaning : "+bestMh.getScore());
			IO.INFO("\n[-------------CLEANING COMPLETED-------------]\n");
		}


		if(mh.cardinality() != 0){
			
			mh.setMotifWidth();}
		if(bestMh.cardinality() != 0){
			bestMh.getListMotif().get(0).setMotifEnd(bestMh.getListMotif().get(0).getMotifStart()+bestMW);
			bestMh.setMotifWidth();
		}

		//Media Delta Score
		if (Debug.ON){ 
			IO.INFO("\n-------------Mean Delta Score (SEQUENCE)-------------\n");
			for(int parte = 0; parte < 3; parte++){
				for(int k = 0; k < media[parte].length; k++){
					media[parte][k] /= count[parte][k];
				}
			}	

			for (int phase = 0; phase<=2; phase++){
				if(phase == 0){
					IO.INFO("\nfase high T-----");
				}else if(phase == 1){
					IO.INFO("\nfase mid T-----");
				}else{
					IO.INFO("\nfase low T-----");
				}
				for (int op = 0; op<=5; op++){
					if (op == 0){
						System.out.print("media add: ");
					}else if(op == 1){
						System.out.print("media remove: ");
					}else if(op == 2){
						System.out.print("media shift: ");
					}else if(op == 3){
						System.out.print("media recalculate: ");
					}else if(op == 4){
						System.out.print("media expand: ");
					}else if(op == 5){
						System.out.print("media shrink: ");
					}
					IO.INFO(media[phase][op] + "\t#operazioni: " + count[phase][op] +
							"\taccept: " + (count[phase][op]-acceptSecondCount[phase][op]-rejectCount[phase][op]) + "\taccept2nd: " + acceptSecondCount[phase][op] + 
							"\treject: " + rejectCount[phase][op]);
				}
			}
		}
		
		IO.INFO("\n----- final local alignment -----\n");

		String fileBench="lastrun_m" + mask + "_run"  + idx ;
		if (clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			fileBench = tmp[tmp.length-1].split("\\.")[0]+ "_m" + mask + "_run"  + idx; //
		}

		IO.INFO("best Mh final check");
		IO.INFO(bestMh.toStringSeq());
		
		String pathBench = output + "/benchmark/mask" + mask + "_nuc/" + fileBench + ".nuc.txt";
		
		IO.writeBenchmark(pathBench, bestMh, escaped, contatore, minSteps, (int)passiTot, "sequence", CIRC, widthUpperBound);
		

		//time Elapsed
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		IO.INFO("run time: " + ((int)elapsedTime/10)/100.0 + " s");

		return new MotifMemory(bestMh.getScore(), bestMh);

	}


	static void refillInput(MotifHandler mh, ArrayList<Motif> inputSequences){
		if (Debug.ON){
			IO.INFO("input size: "+ inputSequences.size());
			IO.INFO("mh size: " + mh.cardinality());
			IO.INFO("\n");
		}
		for (Motif m: mh.getListMotif()){
			inputSequences.add(m);
		}
	}

	public static void main(String[] args) throws IOException {

		//inputSequences
		ArrayList<Motif> inputSequences=new ArrayList<Motif>();


		int numberOfRuns = 1;
		int numberOfMasks=1;
		String baseName = "lastrun";
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)
		if(clp.containsKey("R")){
			numberOfRuns = Integer.parseInt(clp.getValue("R"));
		}
		if(clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			baseName = tmp[tmp.length-1].split("\\.")[0]; //
			IO.readInput(clp.getValue("f"), inputSequences);
		}


		String outFolder="results/" + baseName;
		final File dir = new File(outFolder);
		if(dir.exists()){

			IO.INFO("My scans tell me I already own an output with this name -"+baseName+ "-, want me to overwrite it?");

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String response = bufferedReader.readLine();
			//String response = console.readLine("y/n");
			if(response.toLowerCase().equals("y")){
				IO.INFO("OVERWRITING...");
			}else{
				IO.INFO("CLOSING...");
				System.exit(0);
			}
		}
		String outBench=outFolder+"/benchmark";
		String outScore=outFolder+"/scoreTest";
		String outLogo=outFolder+"/webLogoOut";
		dir.mkdirs();
		new File(outBench).mkdirs();
		new File(outScore).mkdirs();
		new File(outLogo).mkdirs();


		for(int mask=1; mask<=numberOfMasks; mask++){
			for(int i=1; i<=numberOfRuns; i++){
				run(args, i,mask, outFolder, inputSequences);
				IO.INFO("run " + i + " completed");
			}
		}
	}


}

