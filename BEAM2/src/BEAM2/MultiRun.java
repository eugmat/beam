package BEAM2;

//
//Dear maintainer:
//	
//When I wrote this code, only I and Eugenio and God 
//knew what it was. 
//
//Now, only God knows!
//
//So if you are done trying to 'optimize' 
//this routine (and failed),
//please increment the following counter
//as a warning
//to the next guy:
//	
//total_hours_wasted_here = 0
//
//Sincerely, 
//Marco "Noise" Pietrosanto
//

//

//The Main class is now in Parallel.java

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;


class MultiRun extends BEAM2 {
	static void multirun (String[] args, ArrayList<Motif> inputSequences, String baseName, SummarizedData sumData, String backgroundInput,
			String method) throws IOException {
		
		Locale.setDefault(Locale.US);
		//method can be "structure" or "sequence"
		MotifMemory mm = new MotifMemory();
		MotifMemory tmpMm = new MotifMemory();
		boolean CIRC=false;

		int numberOfRuns = 1;
		int numberOfMasks = 1;
		int widthUpperBound = 0; //for CIRC (recomputed in run)
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		//initialized CLP, the hash is created, check with - GetValue(key), ContainsKey(key)
		if(clp.containsKey("R")){
			numberOfRuns = Integer.parseInt(clp.getValue("R"));
		}
		if(clp.containsKey("M")){
			numberOfMasks = Integer.parseInt(clp.getValue("M"));
		}
		if(clp.containsKey("v")){
			Debug.setVERBOSE(true);
		}
		if(clp.containsKey("k")){
			//se esiste -k (keep) allora tieni i risultati di tutte le run
			IO.keepRuns();
		}
		if(clp.containsKey("W")) widthUpperBound=Integer.parseInt(clp.getValue("W")); //limite superiore larghezza motif
		if(clp.containsKey("circ")){
			IO.INFO("circular RNA mode requested");
			CIRC = true; // and modify methods. Expand, reshape, report
			MotifUtilities.circularize(inputSequences, widthUpperBound );
		}


		String outFolder="results/" + baseName;
		String outBench=outFolder+"/benchmark";
		String outBest = outBench + "/motifs";
		String outLogo=outFolder+"/webLogoOut";
		String outBestLogo= outLogo + "/motifs";
		String summaryName= outFolder+"/" + baseName + "_summary.txt";
		if (method=="sequence"){
			summaryName= outFolder+"/" + baseName + "_summary.nuc.txt";
		}

		//




		int bestRunIdx=0;

		long startTimeGlobal = System.currentTimeMillis();




		//Riempio i parametri iniziali per il summary (possibili solo input e bg a questo punto)
		//ma anche i parametri per il bin
		if (method=="structure"){
			sumData.setInputFile(baseName);
		} //do it only for the structure run, the file is the same 
		//WARN if sequence run ends BEFORE the initializing of the structure run -> problem (should not happen btw)
		
		//parametri per il bin di lunghezza e struttura
		sumData.setMl(MotifUtilities.computeMeanLength(inputSequences));
		sumData.setMsc(MotifUtilities.computeMeanStructureContent(inputSequences));






		for(int mask=1; mask<=numberOfMasks; mask++){
			long startTime = System.currentTimeMillis();

			ArrayList<Motif> clonedInput = new ArrayList<Motif>();
			mm=null;
			mm=new MotifMemory();

			tmpMm = null;
			tmpMm =new MotifMemory();


			String outMask = outBench + "/mask" + mask;
			String outMaskLogo = outLogo + "/mask" + mask;
			if (method=="sequence"){
				outMask = outBench + "/mask" + mask + "_nuc";
				outMaskLogo = outLogo + "/mask" + mask + "_nuc";
			}
			new File(outMask).mkdirs();
			new File(outMaskLogo).mkdirs();

			// RUN -------------------------RUN -------------------------RUN -------------------------RUN -------------------------
			for(int i=1; i<=numberOfRuns; i++){

				if (method == "structure"){
					tmpMm=run(args, i, mask, outFolder, inputSequences);
				}else if (method == "sequence"){
					tmpMm=runSequence(args, i, mask, outFolder, inputSequences);
				}
				if (tmpMm==null) return;
				
				//se lo score e' piu' alto, allora metti in cloned le inputSeq (-motifhandl)
				if(mm.tryMask(tmpMm.getScore(), tmpMm.getHandlerMemory(), CIRC )){
					bestRunIdx = i;
					clonedInput = new ArrayList<Motif>(inputSequences.size()); //Se la maschera e' l'attuale migliore, segnati anche lo stato di input sequences
					for(Motif motif: inputSequences){
						clonedInput.add(new Motif(motif));
					}
				}

				if(i != numberOfRuns){ //altrimenti all'ultima run della maschera refilla due volte -- all'ultimo va refillato col migliore
					refillInput(tmpMm.getHandlerMemory(), inputSequences);
				}

				//outFolder � basename/
				if (method=="sequence"){
					IO.INFO("mask: " + mask +"\trun: " + i + "\tcompleted [nucleotides] ");
				}else{
					IO.INFO("mask: " + mask +"\trun: " + i + "\tcompleted ");
				}
			}
			//in bestRunIdx e' contenuto l'indice della run da copiare, in mask c'e' invece il numero di maschera





			//applyMask(mm.getHandlerMemory());
			refillInput(mm.getHandlerMemory(), clonedInput); //Applica la maschera ai Motif del migliore mh e riempi l'inputSequences associato ad esso con i motif 

			//aggiornati

			//Dopo aver deciso le migliori...
			//SEARCH -- dopo il refill cos� il background pu� essere composto
			//facilmente anche delle input refillate
			MotifHandler searchMh = new MotifHandler();

			//BACKGROUND ------------------------------------BACKGROUND ------------------------------------BACKGROUND ------------------------------------BACKGROUND ------------------------------------
			//if CIRC ----
			//TODO CIRC
			if(!backgroundInput.equals("")){
				String backgroundFile = backgroundInput;
				String searchBenchName = outBest + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".search.txt";
				if (method=="sequence"){
					searchBenchName = outBest + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + ".search.nuc.txt";
				}

				//Create search files
				KS ks = new KS(); //Data for KS test
				IO.INFO("Refining the model...");
				double threshold = SearchAfterRun.searchAftRun(mm.getHandlerMemory(), backgroundFile, searchBenchName, clonedInput, searchMh, method, ks, CIRC, widthUpperBound);	
				//ks filled with bg data
				
				//weblogo after search
				String webLogoOut = "";
				if (method=="sequence"){
					webLogoOut = outMaskLogo + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.nuc.fa";
				}else{
					webLogoOut = outMaskLogo + "/" + baseName + "_m" + mask + "_run" + bestRunIdx + "_wl.fa";
				}
				IO.writeWebLogo(webLogoOut, searchMh, method);
				
				
				//NOTA: in questo modo se in futuro voglio tener conto per la mask anche delle
				//sequenze aggiunte con search NON posso perch� ho gia fatto il refill.
				//per� questa scelta mi serve per montare facilmente input + bg
				IO.INFO("Doing statistics...");

				SearchObj sObj = IO.readSearchOutput(searchBenchName, clonedInput.size(), ks.getBackground_partials().size(), method, ks );
				//ks filled with foreground data

				String gaussFile = searchBenchName + ".gauss";

				double cov = sObj.getTP()*1.0 / (sObj.getTP()+ sObj.getFN());
				MotifUtilities.computePvalue(gaussFile, sObj.getMeanPart(), ks, cov);

				sObj.setPvalueMW(ks.getpMW());
				sObj.setStatisticMW(ks.getStatisticMW());
				sObj.setPvalueT(ks.getPvalue());
				sObj.setPvalueHG(ks.getpHG());
				sObj.setStatisticT(ks.getStatistic());
				sObj.setAUCMW(ks.getAUCMW());
				sObj.setCohen(ks.getCohen());
				sObj.setCohenu3(ks.getCohenu3());
				
				//Aggiornamento sumData
				sumData.setMCC(sObj.computeMCC());
				sumData.setScore(mm.getHandlerMemory().getScore());
				
				sumData.setConsensus(sObj.computeConsensus());
				sumData.setqbearConsensus(sObj.computeqbearConsensus());

				sumData.setInputSize(sObj.getTP() + sObj.getFN());
				sumData.setBgSize(sObj.getFP()+sObj.getTN());
				sumData.setMask(mask);
				sumData.setCoverage(cov) ;
				sumData.setFallout(sObj.getFP()*1.0/ (sObj.getFP()+sObj.getTN()));
				sumData.setAcc((sObj.getTP() + sObj.getTN())*1.0/(sObj.getTP()+ sObj.getFN()+sObj.getFP()+sObj.getTN()));


				long elapsedTime = System.currentTimeMillis() - startTime;
				
				IO.appendToSummary(summaryName, sObj, sumData, elapsedTime, method);
			}

			//la mask la applico dopo il search
			if(mask != numberOfMasks){ 
				IO.INFO("applying mask");
			}
			if(CIRC){
				MotifUtilities.applyMaskCirc(searchMh, widthUpperBound);
			}else{
				MotifUtilities.applyMask(searchMh);
			}

			inputSequences = null; //svuota inputSequences e riempila copiando gli elementi di ClonedInput
			inputSequences = new ArrayList<Motif>();
			//prova con searchmh, dopo bisogner� integrare con quelli rimasti fuori
			for(Motif motif: searchMh.getListMotif()){
				if(!motif.getName().endsWith("_bg")){
					inputSequences.add(motif);
					//IO.INFO(motif.getName());
				}
			}
			for(Motif motif: clonedInput){
				if(!inputSequences.contains(motif)){

					inputSequences.add(new Motif(motif));
					//cc
				}
				/*
				else{
					IO.INFO("ho gi� " + motif.getName() + ", con maschera " + motif.printMask());
				}//QUAQUA */
			}
			//IO.INFO("in partenza avevo " + startingSize + ", ora ho " + inputSequences.size());

			//COPIA cartelle
			IO.moveBestRuns(baseName, mask, bestRunIdx, outBench, method);
			IO.moveBestRunsLogo(baseName, mask, bestRunIdx, outLogo, method);
			//weblogo
			Weblogo weblogo = new Weblogo();
			try {
				weblogo.generateLogo(baseName, mask, bestRunIdx, outLogo);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//TODO
			IO.appendToBenchmark(baseName,mask,bestRunIdx);

			//Cancella cartelle inutili se non � stato richiesto di mantenerle
			if( !IO.getKEEP() && method == "structure" ){
				for (int maskIdx=1; maskIdx<=numberOfMasks;maskIdx++){
					boolean parent = true;
					IO.deleteFolder(outBench + "/mask" + maskIdx, parent);
					IO.deleteFolder(outLogo + "/mask" + maskIdx, parent);
				}
			}
			if( !IO.getKEEP() && method == "sequence" ){
				for (int maskIdx=1; maskIdx<=numberOfMasks;maskIdx++){
					boolean parent = true;
					IO.deleteFolder(outBench + "/mask" + maskIdx + "_nuc", parent);
					IO.deleteFolder(outLogo + "/mask" + maskIdx + "_nuc", parent);
				}
			}


		}


		long stopTime = System.currentTimeMillis();
		long elapsedTimeGlobal = stopTime - startTimeGlobal;
		IO.INFO("full run time: " + ((int)elapsedTimeGlobal/10)/100.0 + " s");
		IO.INFO("starting next step...");

	}//fine main


	public static void main(String[] args) throws IOException {
		//DEPRECATED, USE PARALLEL MAIN
		ArrayList<Motif> inputSequences=new ArrayList<Motif>();
		ArrayList<Motif> inputSequences_nuc=new ArrayList<Motif>();
	
		String baseName = "lastrun";
		String backgroundInput = ""; //DEFAULT rfam?
		boolean USERBG=false;
		boolean nucleoFlag=false;
		SummarizedData sumData = new SummarizedData();
		//genero inputSequences
		
		//CommandLine Parser 
		CommandLineParser clp = new CommandLineParser(args);
		if(clp.containsKey("f")){
			String[] tmp =  clp.getValue("f").split("/");
			baseName = tmp[tmp.length-1].split("\\.")[0]; //
			IO.readInput(clp.getValue("f"), inputSequences);
			IO.readInput(clp.getValue("f"), inputSequences_nuc);
	
		}
		if(clp.containsKey("g")){
			backgroundInput = clp.getValue("g");
			USERBG = true;
		}
		if(clp.containsKey("N")){
			nucleoFlag=true;
		}
		String outFolder="results/" + baseName;
	
		
		final File dir = new File(outFolder);
		if(dir.exists()){
			boolean parent = false;
			IO.deleteFolder(outFolder, parent);
		}
		
		String outBench=outFolder+"/benchmark";
		String outBest = outBench + "/motifs";
		String outLogo=outFolder+"/webLogoOut";
		String outBestLogo= outLogo + "/motifs";
		dir.mkdirs();
	
	
		new File(outBench).mkdirs();
		new File(outBest).mkdirs();
		new File(outLogo).mkdirs();
		new File(outBestLogo).mkdirs();
		
		if(!USERBG){
			//se non e' presente un background inserito dall'utente, prova comunque a stimarlo
			//da Rfam, con i criteri di LSbin
			String bin=sumData.computeBin();
	
			//backgroundInput = IO.defaultBgCreation(bin, inputSequences.size(), outFolder);
			//in assunzione gaussiana della distribuzione degli score va bene anche se uso sempre 250
			backgroundInput = IO.defaultBgCreation(bin, 250, outFolder);			
	
		}
		
		String[] bgTmp = backgroundInput.split("/");
		sumData.setBgFile(bgTmp[bgTmp.length - 1].split("\\.")[0]);
		//NOTA: sono anche gli unici fissi (gli altri cambiano a ogni run)
		
		//Parallelize!
		
		
		multirun(args, inputSequences, baseName, sumData, backgroundInput, "structure");
		if (nucleoFlag){
		multirun(args, inputSequences_nuc, baseName, sumData, backgroundInput, "sequence");
		}
		//in this way it should be parallelizable
	
		//Post analysis: concurrency, mutual info
		//MutualInfo.postAnalysis(outBest);
		
	
	}

}
