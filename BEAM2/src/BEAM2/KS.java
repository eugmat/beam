package BEAM2;

import java.util.ArrayList;

import org.apache.commons.math3.exception.NoDataException;

class KS {
	private ArrayList<Double> foreground_partials;
	private ArrayList<Double> background_partials;
	private ArrayList<Double> best_fore_partials;
	private double pvalue;
	private double statistic;
	private double statisticMW;
	private double AUCMW;
	private double pMW;
	private double HG;
	private double pHG;
	private double cohen;
	private double cohenu3; //percentile gain (1-pvalue)
	private int sampleSize;
	KS(){
		this.background_partials=new ArrayList<Double>();
		this.foreground_partials=new ArrayList<Double>();
		this.pvalue=-1.0;
		this.statistic=-10.0;
		this.statisticMW=-11.0;
		this.AUCMW=-12.0;
		this.pMW=-2.0;
		this.pHG=-3.0;

		this.cohen=-1000.0;
		this.cohenu3=-1100.0;


	}

	public ArrayList<Double> getBackground_partials() {
		return background_partials;
	}

	public void setBackground_partials(ArrayList<Double> background_partials) {
		if(background_partials.size() == 0){
			throw new NoDataException();
		}
		this.background_partials = background_partials;
	}

	public ArrayList<Double> getForeground_partials() {
		return foreground_partials;
	}

	public void setForeground_partials(ArrayList<Double> foreground_partials) {
		this.foreground_partials = foreground_partials;
	}
	public void setForeground_partials(MotifHandler mh) {	
		for(Motif m:mh.getListMotif()){
			this.foreground_partials.add(m.getPartial());
		}
	}
	
	public void setBestFore_partials(ArrayList<Double> best_fore_partials) {
		this.best_fore_partials = best_fore_partials;
		
	}

	public ArrayList<Double> getBest_fore_partials() {
		return best_fore_partials;
	}

	public double getPvalue() {
		return pvalue;
	}

	public void setPvalue(double pvalue) {
		this.pvalue = pvalue;
	}

	public double getStatistic() {
		return statistic;
	}

	public void setStatistic(double statistic) {
		//generic
		this.statistic = statistic;
	}

	//MannWithneyU
	public double getStatisticMW() {
		return statisticMW;
	}
	public void setStatisticMW(double statisticMW) {
		this.statisticMW = statisticMW;
	}
	

	public double getpMW() {
		return pMW;
	}

	public void setpMW(double pMW) {
		this.pMW = pMW;
	}

	public double getAUCMW() {
		return AUCMW;
	}
	
	public void setAUCMW(double AUCMW) {
		this.AUCMW = AUCMW;
	}

	public double getpHG() {
		return pHG;
	}

	public void setpHG(double pHG) {
		this.pHG = pHG;
	}

	public double getCohen() {
		return cohen;
	}

	public void setCohen(double cohen) {
		this.cohen = cohen;
	}

	public double getCohenu3() {
		return cohenu3;
	}

	public void setCohenu3(double cohenu3) {
		this.cohenu3 = cohenu3;
	}

	public void setSampleSize(int sampleSize) {
		this.sampleSize=sampleSize;
	}

	public int getSampleSize() {
		return sampleSize;
	}

}
