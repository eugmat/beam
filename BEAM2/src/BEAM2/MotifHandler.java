package BEAM2;

import java.util.ArrayList;
//import java.util.Iterator;

class MotifHandler{
	private ArrayList<Motif> motifList;
	private ArrayList<Motif> otherMatches;

	private PSSM pssm, qBEARPssm;
	private int motifWidth;
	private int motifWidthPrev;
	private int shortestSequenceLength;
	private int shortestSequenceLengthPrev;
	//lunghezza sequenza piu' corta.

	public MotifHandler(){
		this.motifList=new ArrayList<Motif>();
		this.otherMatches=new ArrayList<Motif>();

		this.motifWidth=0;
		this.motifWidthPrev=0;
		this.pssm=new PSSM();
		this.qBEARPssm = new PSSM();
	}

	void MotifHandlerClone(MotifHandler copy){
		this.motifList = new ArrayList<Motif>();
		this.otherMatches = new ArrayList<Motif>();

		for (Motif m : copy.motifList){

			Motif addM = new Motif(m);
			//			IO.INFO(m.getName() +"\t" +addM.getIndex() + "\t precedente: " +m.getIndex());

			this.motifList.add(addM);
		}

		for (Motif m : copy.otherMatches){

			Motif addM = new Motif(m);
			//			IO.INFO(m.getName() +"\t" +addM.getIndex() + "\t precedente: " +m.getIndex());

			this.otherMatches.add(addM);
		}
		this.motifWidth=copy.motifWidth;
		this.motifWidthPrev=copy.motifWidthPrev;
		this.pssm=copy.pssm;
		this.qBEARPssm = copy.qBEARPssm;
		this.shortestSequenceLength=copy.shortestSequenceLength;
		this.shortestSequenceLengthPrev=copy.shortestSequenceLengthPrev;
	}

	public MotifHandler add(MotifHandler adding){
		//adds a list of motifs from a mh to a base mh (which retains all fields)
		MotifHandler tmp = new MotifHandler();
		tmp.MotifHandlerClone(this);
		for (Motif m: adding.motifList){
			tmp.addMotif(m);
		}
		return tmp;
	}

	void addMotif(String name, String nuc, String seq, int s, int e){
		Motif tmp=new Motif(name, nuc, seq,s,e);
		this.motifList.add(tmp);
	}

	void addMotif(Motif m){
		this.motifList.add(m);
	}

	public void addMotif(Motif m, int index){
		this.motifList.add(index,m);
	}

	void addOtherMatch(Motif m){
		this.otherMatches.add(m);
	}

	void removeMotif(int index){
		this.motifList.remove(index);
	}	

	void setMotifWidth() {
		this.motifWidthPrev=this.motifWidth;
		this.motifWidth =this.motifList.get(0).getMotifEnd() - this.motifList.get(0).getMotifStart();
		if (this.motifWidth < 0){
			this.motifWidth=this.motifWidthPrev;
		}
	}

	public void setMotifWidth(int motifWidth) {
		this.motifWidthPrev=this.motifWidth;
		this.motifWidth = motifWidth;
		if (this.motifWidth < 0){
			this.motifWidth=this.motifWidthPrev;
		}
	}

	public int getMotifWidth() {
		return this.motifWidth;
	}

	public int getMotifWidthPrev() {
		return this.motifWidthPrev;
	}


	Motif getObjectMotif(int index){
		return this.motifList.get(index);
	}

	public ArrayList<Motif> getListMotif(){
		return this.motifList;
	}

	public ArrayList<Motif> getOtherMatches() {
		return this.otherMatches;
	}

	String getStructureMotif(int i){
		return this.motifList.get(i).extractMotifFromStructure();
	}

	String getNucleotideMotif(int i){
		return this.motifList.get(i).extractMotifFromNucleotides();
	}
	public String getDBMotif(int i){
		return this.motifList.get(i).extractMotifFromDB();
	}

	int cardinality(){
		return this.motifList.size();
	}

	String printPSSM(){
		return this.pssm.toString(this.motifWidth);
	}



	public String printQBEARPSSM(){
		return this.qBEARPssm.toString(this.motifWidth);
	}

	public double getScore(){
		return this.pssm.getScore();
	}

	static void computeScore(MotifHandler mh){

		mh.getPSSM().setScore(PSSM.ScorePartials(mh));

	}

	public PSSM getPSSM() {
		return pssm;
	}

	public PSSM getQBEARPSSM() {
		return qBEARPssm;
	}

	@Override
	public String toString(){
		String motifString = "";
		if (Debug.ON){
			for (int i = 0; i < cardinality(); i++) {
				motifString += getStructureMotif(i)+"\t" + getObjectMotif(i).getName() + 
						"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  getObjectMotif(i).getStructure().length() + 
						"\t" + getObjectMotif(i).getMotifStart()+ "\t" + getObjectMotif(i).getMotifEnd() + 
						"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
						"\t" + getObjectMotif(i).printMask() +
						"\n";
			}
		}else{
			for (int i = 0; i < cardinality(); i++) {
				motifString += getStructureMotif(i)+"\t" + getObjectMotif(i).getName() + 
						"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  getObjectMotif(i).getStructure().length() + 
						"\t" + getObjectMotif(i).getMotifStart()+ "\t" + getObjectMotif(i).getMotifEnd() + 
						"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
						"\n";
			}
		}
		return motifString;
	}

	String toStringCirc(int maxWidth) {
		String motifString = "";

		for (int i = 0; i < cardinality(); i++) {

			int extension= Math.min(this.getObjectMotif(i).getDotBracket().length(), Math.max(50, maxWidth));
			//dotbracket is left untouched (original length)
			int start = getObjectMotif(i).getMotifStart();
			int end = getObjectMotif(i).getMotifEnd();
			int leng = this.getObjectMotif(i).getDotBracket().length();

			start=MotifUtilities.decircularize(start,extension,leng);
			end=MotifUtilities.decircularize(end,extension,leng);

			motifString += getStructureMotif(i)+"\t" + getObjectMotif(i).getName() + 
					"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  leng + 
					"\t" + start+ "\t" + end + 
					"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
					"\n";
		}

		return motifString;
	}

	public String toString(boolean webLogo, boolean qBEARFlag){
		//in questa versione con due parametri, (webLogo) stampa il fasta, qBEARFlag converte in qBEAR
		String motifString = "";
		if(qBEARFlag){
			for (int i = 0; i < cardinality(); i++) {
				motifString += ">" + getObjectMotif(i).getName() +"\n";
				for (int j = 0; j < getStructureMotif(i).length() ; j++){
					motifString += qBEAR.identifyClass(getStructureMotif(i).charAt(j));
				}
				motifString += "\n";
			}
		}else{
			for (int i = 0; i < cardinality(); i++) {
				motifString += ">" + getObjectMotif(i).getName() +"\n" + getStructureMotif(i)+"\n";
			}
		}
		return motifString;
	}

	String toStringWebLogo(boolean webLogo, boolean qBEARFlag){
		//in questa versione con due parametri, (webLogo) stampa il fasta, qBEARFlag converte in qBEAR
		String motifString = "";
		if(qBEARFlag){
			for (int i = 0; i < cardinality(); i++) {
				if(!getObjectMotif(i).getName().endsWith("_bg")){

					motifString += ">" + getObjectMotif(i).getName() +"\n";
					for (int j = 0; j < getStructureMotif(i).length() ; j++){
						motifString += qBEAR.identifyClass(getStructureMotif(i).charAt(j));
					}
					motifString += "\n";
				}
			}
		}else{

			for (int i = 0; i < cardinality(); i++) {
				if(!getObjectMotif(i).getName().endsWith("_bg")){

					motifString += ">" + getObjectMotif(i).getName() +"\n" + getStructureMotif(i)+"\n";
				}
			}
		}
		return motifString;
	}

	String otherMatchesToString() {
		String motifString = "";

		for (Motif m: this.otherMatches) {
			try{
				motifString += m.extractMotifFromStructure()+"\t" + m.getName() + 
						"$"+ (m.getIndex()+1) +  "\t" +  m.getStructure().length() + 
						"\t" + m.getMotifStart()+ "\t" + m.getMotifEnd() + 
						"\t" +  MotifUtilities.truncateDecimal(m.getPartial(), 4) + 
						"\n";
			}catch(Exception e){

			}
		}

		return motifString;
	}

	String otherMatchesToStringCirc(int maxWidth) {
		String motifString = "";

		for (Motif m: this.otherMatches) {
			try{
				
					int extension= Math.min(m.getDotBracket().length(), Math.max(50, maxWidth));
					//dotbracket is left untouched (original length)
					int start = m.getMotifStart();
					int end = m.getMotifEnd();
					int leng = m.getDotBracket().length();
					start=MotifUtilities.decircularize(start,extension,leng);
					end=MotifUtilities.decircularize(end,extension,leng);
				
				motifString += m.extractMotifFromStructure()+"\t" + m.getName() + 
						"$"+ (m.getIndex()+1) +  "\t" +  m.getStructure().length() + 
						"\t" + start+ "\t" + end + 
						"\t" +  MotifUtilities.truncateDecimal(m.getPartial(), 4) + 
						"\n";
			}catch(Exception e){

			}
		}

		return motifString;
	}

	String otherMatchesToStringSeq() {
		String motifString = "";

		for (Motif m: this.otherMatches) {
			try{
				motifString += m.extractMotifFromNucleotides()+"\t" + m.getName() + 
						"$"+ (m.getIndex()+1) +  "\t" +  m.getStructure().length() + 
						"\t" + m.getMotifStart()+ "\t" + m.getMotifEnd() + 
						"\t" +  MotifUtilities.truncateDecimal(m.getPartial(), 4) + 
						"\n";
			}catch(Exception e){

			}
		}

		return motifString;
	}

	String otherMatchesToStringSeqCirc(int maxWidth) {
		String motifString = "";

		for (Motif m: this.otherMatches) {
			try{
				int extension= Math.min(m.getDotBracket().length(), Math.max(50, maxWidth));
				//dotbracket is left untouched (original length)
				int start = m.getMotifStart();
				int end = m.getMotifEnd();
				int leng = m.getDotBracket().length();
				start=MotifUtilities.decircularize(start,extension,leng);
				end=MotifUtilities.decircularize(end,extension,leng);
				
				motifString += m.extractMotifFromNucleotides()+"\t" + m.getName() + 
						"$"+ (m.getIndex()+1) +  "\t" +  leng + 
						"\t" + start+ "\t" + end + 
						"\t" +  MotifUtilities.truncateDecimal(m.getPartial(), 4) + 
						"\n";
			}catch(Exception e){

			}
		}

		return motifString;
	}

	public String toStringSeq(boolean webLogo){
		//in questa versione con parametro, (webLogo) stampa il fasta
		String motifString = "";

		for (int i = 0; i < cardinality(); i++) {
			motifString += ">" + getObjectMotif(i).getName() +"\n" + getNucleotideMotif(i)+"\n";

		}
		return motifString;
	}

	String toStringSeqWebLogo(boolean webLogo){
		//in questa versione con parametro, (webLogo) stampa il fasta, does not print _bg
		String motifString = "";

		for (int i = 0; i < cardinality(); i++) {
			if(!getObjectMotif(i).getName().endsWith("_bg")){
				motifString += ">" + getObjectMotif(i).getName() +"\n" + getNucleotideMotif(i)+"\n";
			}
		}
		return motifString;
	}


	public String toStringpBear(boolean webLogo){
		//in questa versione con due parametri, (webLogo) stampa il fasta pBear
		String motifString = "";

		for (int i = 0; i < cardinality(); i++) {
			motifString += ">" + getObjectMotif(i).getName() +"\n";
			for (int j = 0; j < getStructureMotif(i).length() ; j++){
				//TODO motifString += qBEAR.PartialIdentifyClass(getSequenceMotif(i).charAt(j));
			}
			motifString += "\n";
		}

		for (int i = 0; i < cardinality(); i++) {
			motifString += ">" + getObjectMotif(i).getName() +"\n" + getStructureMotif(i)+"\n";
		}

		return motifString;
	}

	public String toStringTestRegex(boolean webLogo, boolean qBEARFlag){
		//in questa versione con due parametri, (webLogo) stampa il fasta, qBEARFlag converte in qBEAR
		String motifString = "";
		if(qBEARFlag){
			for (int i = 0; i < cardinality(); i++) {
				motifString += getObjectMotif(i).getName() +"\t";
				motifString += getObjectMotif(i).getMotifStart() + "\t" + getObjectMotif(i).getMotifEnd();

				motifString += "\n";
			}
		}else{
			for (int i = 0; i < cardinality(); i++) {
				motifString += ">" + getObjectMotif(i).getName() +"\n" + getStructureMotif(i)+"\n";
			}
		}
		return motifString;
	}

	public void setShortestSequenceLength(int shortestLength) {
		this.shortestSequenceLength = shortestLength;
	}

	public int getShortestSequenceLength() {
		return this.shortestSequenceLength;
	}

	public void setShortestSequenceLengthPrev(int shortestLength) {
		this.shortestSequenceLengthPrev = shortestLength;
	}

	public int getShortestSequenceLengthPrev() {
		return this.shortestSequenceLengthPrev;
	}

	String toStringSeq() {
		//stampa il motifhandler di sequenza
		String motifString = "";
		int i=0;
		try{
			for (i = 0; i < cardinality(); i++) {
				if(getObjectMotif(i).getNucleotides().equals("") == false){
					motifString += getObjectMotif(i).getNucleotides().substring(getObjectMotif(i).getMotifStart(), getObjectMotif(i).getMotifEnd())+"\t" + getObjectMotif(i).getName() + 
							"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  getObjectMotif(i).getStructure().length() + 
							"\t" + getObjectMotif(i).getMotifStart()+ "\t" + getObjectMotif(i).getMotifEnd() + 
							"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
							"\n";
				}
			}
		}catch(StringIndexOutOfBoundsException e){
			IO.ERR("\nerror in sequence no.\t" + i + "\n");
			e.printStackTrace();
		}
		return motifString;
	}
	
	String toStringSeqCirc( int maxWidth) {
		//stampa il motifhandler di sequenza
		String motifString = "";
		int i=0;
		try{
			
			
			for (i = 0; i < cardinality(); i++) {
				

				
				if(getObjectMotif(i).getNucleotides().equals("") == false){
					int extension= Math.min(this.getObjectMotif(i).getDotBracket().length(), Math.max(50, maxWidth));
					//dotbracket is left untouched (original length)
					int start = getObjectMotif(i).getMotifStart();
					int end = getObjectMotif(i).getMotifEnd();
					int leng = this.getObjectMotif(i).getDotBracket().length();
					
					start=MotifUtilities.decircularize(start,extension,leng);
					end=MotifUtilities.decircularize(end,extension,leng);
					
					motifString += getObjectMotif(i).getNucleotides().substring(getObjectMotif(i).getMotifStart(), getObjectMotif(i).getMotifEnd())+"\t" + getObjectMotif(i).getName() + 
							"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  leng + 
							"\t" + start+ "\t" + end + 
							"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
							"\n";
				}
			}
		}catch(StringIndexOutOfBoundsException e){
			IO.ERR("\nerror in sequence no.\t" + i + "\n");
			e.printStackTrace();
		}
		return motifString;
	}

	String toStringDB() {
		//stampa il motifhandler di DB
		String motifString = "";
		int i=0;
		try{
			for (i = 0; i < cardinality(); i++) {
				
				
				if(getObjectMotif(i).getDotBracket().equals("") == false){
					motifString += getObjectMotif(i).getDotBracket().substring(getObjectMotif(i).getMotifStart(), getObjectMotif(i).getMotifEnd())+"\t" + getObjectMotif(i).getName() + 
							"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  getObjectMotif(i).getStructureList().size() + 
							"\t" + getObjectMotif(i).getMotifStart()+ "\t" + getObjectMotif(i).getMotifEnd() + 
							"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
							"\n";
				}
			}
		}catch(StringIndexOutOfBoundsException e){
			IO.ERR("\nerror in sequence no.\t" + i + "\n");
			e.printStackTrace();
		}
		return motifString;
	}

	String toStringDBCirc(int maxWidth) {
		//stampa il motifhandler di DB decircolarizzato
		String motifString = "";
		int i=0;
		try{
			for (i = 0; i < cardinality(); i++) {
				
				
				if(getObjectMotif(i).getDotBracket().equals("") == false){
					int extension= Math.min(this.getObjectMotif(i).getDotBracket().length(), Math.max(50, maxWidth));
					//dotbracket is left untouched (original length)
					int start = getObjectMotif(i).getMotifStart();
					int end = getObjectMotif(i).getMotifEnd();
					int leng = this.getObjectMotif(i).getDotBracket().length();
					
					start=MotifUtilities.decircularize(start,extension,leng);
					end=MotifUtilities.decircularize(end,extension,leng);
					
					//ATTENZIONE C'E' UN SUBSTRING TODO QUA VA STUDIATO COME GIRARE LE PARENTESI
					String db = null;
					if(start > end){
						db=getObjectMotif(i).getDotBracket().substring(start) +  getObjectMotif(i).getDotBracket().substring(0, end);
					}else{
						db=getObjectMotif(i).getDotBracket().substring(start, end);
					}
					
					motifString += db+"\t" + getObjectMotif(i).getName() + 
							"$"+ (getObjectMotif(i).getIndex()+1) +  "\t" +  leng + 
							"\t" + start+ "\t" + end + 
							"\t" +  MotifUtilities.truncateDecimal(getObjectMotif(i).getPartial(), 4) + 
							"\n";
				}
			}
		}catch(StringIndexOutOfBoundsException e){
			IO.ERR("\nerror in sequence no.\t" + i + "\n");
			e.printStackTrace();
		}
		return motifString;
	}

	public double getMinPartial() {
		//restituisce il minimo valore tra tutti i parziali di un mh
		double min = 999999.9;
		for (Motif m: this.motifList){
			if(m.getPartial() < min){
				min= m.getPartial();
			}
		}
		return min;
	}

	public double getMaxPartial() {
		//restituisce il max valore tra tutti i parziali di un mh
		double max = 0;
		for (Motif m: this.motifList){
			if(m.getPartial() > max){
				max= m.getPartial();
			}
		}
		return max;
	}

	void adjustEndIndexes(int motifWidth) {
		// mette apposto gli end indexes che ogni tanto paiono scomparire
		for(Motif m:this.motifList){
			m.setMotifEnd(m.getMotifStart()+ motifWidth);
		}

	}








}
