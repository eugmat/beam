package BEAM2;

import java.util.ArrayList;

class SearchAfterRun {
	// Cerca benchmark in fasta(background + input)

	static double searchAftRun(MotifHandler mh, String backgroundFile, String benchSearchFile, ArrayList<Motif> inputSequences, MotifHandler searchMh, String method, KS ks, boolean CIRC, int maxWidth) {
		//cerca mh (PSSM) in backgroundFile+input (query)
		//restituisce grandezza background (serve per l'MCC)
		
		ArrayList<Double> background_partials = new ArrayList<Double>(); //for KS
		
		long startTime = System.currentTimeMillis();

		//DONE 1 - con questa modifica la lettura di mbr passa da 100ms a 3ms -> su 10^6 calls
		// risparmia ~ 1g 
		double [][]mat=null;
		if (method=="sequence"){
			mat=SubsMat.getNuclSubsMat();
		}else{
			mat=SubsMat.getmbr();
		}

		//cose per il calcolo del pvalue
		double gaussMean=0.0, gaussVar =0.0;
		int gaussCounter = 0;

		//Qua Query = TARGET
		ArrayList<Motif> querySequences=new ArrayList<Motif>();
		//riempie querySequences (i fasta in cui cercare)
		//Negatives
		boolean bgFlag = true; //formale, per readability. E perch� java � na merda e non supporta i parametri opzionali.
		IO.readInputForSearch(backgroundFile,querySequences, bgFlag); 
	
		if(CIRC){
			MotifUtilities.circularize(querySequences, maxWidth);
		}
		
		querySequences.size();
		

		
		for(Motif m: inputSequences){
			querySequences.add(m);

		}
		//querySequences contiene bg + input rimanente

		//per sicurezza setto di nuovo il parametro width del motivo (dovrebbe gi� esserci)
		mh.setMotifWidth();		

		//genero la PSSM (come sopra dovrebbe gi� esserci)
		if (method=="sequence"){
			PSSM.computePSSM_Nucleotides(mh, new ArrayList<Motif>());
			MotifUtilities.computePartials_Nucleotides(mh.getPSSM(), mh, mat, mh.getMotifWidth());

		}else{
			PSSM.computePSSM(mh, new ArrayList<Motif>());
			MotifUtilities.computePartials(mh.getPSSM(), mh, mat, mh.getMotifWidth());
		}
		//leggo il max/min partial che c'� nel bm (sar� la threshold con cui cercare)
		double min = mh.getMinPartial();
		double max = mh.getMaxPartial();

		//double thr = .9*max;
		double thr = min;
		double thrSeq = min; //no more than 2 mismatch allowed (on 3,-2) (min-11, but this caused too many unwanted to be brought in) 


		//Inizio ricerca su sequenze query
		//Nel mentre mi segno i parziali che escono dai background per avere gaussian dei parziali
		//che servira' per avere Z-score della media del modello, che -> p-value
		double bestMatch = 0.0;
		int counter = 0;
		for(Motif m: querySequences){
			bestMatch=0.0;

			//bisogna ciclare su le sequenze di querySequences (e per ogni sequenza su ogni subopt)
			//ciclo su sequenze
			if (method=="sequence"){
				thr = thrSeq;
				if (m.getNucleotides().length() >= mh.getMotifWidth()){
					bestMatch=MotifUtilities.computeScoreVsPSSM_Nucleotides(mh.getPSSM(), m, mat, mh.getMotifWidth());
					//SEZIONE per gaussian modeling

					if (m.getName().endsWith("_bg")){
						gaussMean+=bestMatch;
						gaussVar+=bestMatch*bestMatch;
						gaussCounter+=1;
						background_partials.add(bestMatch);
					}
					
					m.setPartial(bestMatch); //do this anyway

					if (bestMatch >= thrSeq){
						//se � accettabile a questo punto la devo integrare all'mh
						mh.addMotif(m); 
						//NOTARE che start ed end sono gi� inclusi nell'oggetto Motif (sono settati da computeScoreEtc...)
						searchMh.addMotif(m);
						//IO.INFO(">" + m.getName());
						//IO.INFO(m.extractMotifFromSequence() + "\t" + m.getMotifStart() + "\t" + m.getMotifEnd() + "\t" + bestMatch);
						counter++;
					}else{//bestMatch < thrSeq
						searchMh.addOtherMatch(m);
					}
				}else{//the motif is too long for this sequence -- set 0.0
					if (m.getName().endsWith("_bg")){
						/*
						gaussMean+=0.0;
						gaussVar+=0.0*0.0;
						*/
						gaussCounter+=1;
						background_partials.add(0.0);
					}
					m.setPartial(0.0);
					gaussCounter+=1;
					//searchMh.addOtherMatch(m);
				}
			}else{//method=="structure"
				if (m.getStructure().length() >= mh.getMotifWidth()){		
					bestMatch=MotifUtilities.computeScoreVsPSSM(mh.getPSSM(), m, mat, mh.getMotifWidth());
					bestMatch=MotifUtilities.computeScoreVsPSSM_allSubopt(mh.getPSSM(), m, mat, mh.getMotifWidth());

					//SEZIONE per gaussian modeling
					if (m.getName().endsWith("_bg")){
						gaussMean+=bestMatch;
						gaussVar+=bestMatch*bestMatch;
						gaussCounter+=1;
						background_partials.add(bestMatch);
					}
					
					m.setPartial(bestMatch); //do this anyway

					if (bestMatch >= thr){
						//se � accettabile a questo punto la devo integrare all'mh
						mh.addMotif(m); 
						//NOTARE che start ed end sono gi� inclusi nell'oggetto Motif (sono settati da computeScoreEtc...)
						searchMh.addMotif(m);
						//IO.INFO(">" + m.getName());
						//IO.INFO(m.extractMotifFromSequence() + "\t" + m.getMotifStart() + "\t" + m.getMotifEnd() + "\t" + bestMatch);
						counter++;
					}else { //under threshold
						searchMh.addOtherMatch(m);
					}
				}
			}
		}
		IO.INFO("\n#Search completed");
		IO.INFO("#seq query: " + counter);
		IO.INFO("#on: " + querySequences.size() + " RNAs (input + bg).");

		ArrayList<Motif> dump = new ArrayList<Motif>();
		//in dump ci vanno le sequenze che causano errori, magari pu� servire
		if( counter != 0){
			searchMh.setMotifWidth();
			if (method == "sequence"){
				PSSM.computePSSM_Nucleotides(searchMh, dump);
				IO.writeBenchmark(benchSearchFile, searchMh, true, -1, -1, -1, method, CIRC, maxWidth);
			}else{
				PSSM.computePSSM(searchMh, dump);
				IO.writeBenchmark(benchSearchFile, searchMh, true, -1, -1, -1, CIRC, maxWidth);
			}
		}else{
			IO.writeFailedSearchBenchmark(benchSearchFile);
		}
		//ora devo stamparla in un file

		long stopTime = System.currentTimeMillis();
		///*
		long elapsedTime = stopTime - startTime;
		System.out.print("Search - elapsed Time(ms):\t" + elapsedTime + "\n");
		System.out.print("min partial:\t" + min + "\n");
		System.out.print("max partial:\t" + max + "\n");
		System.out.print("dump size:\t" + dump.size() + "\n");

		//devo gestire il calcolo del pvalue
		gaussMean /= gaussCounter;
		gaussVar /= gaussCounter;
		gaussVar -= gaussMean*gaussMean;

		ks.setSampleSize(searchMh.cardinality());
		String gaussfile=benchSearchFile+".gauss";
		IO.writeGaussian(gaussfile,gaussMean,gaussVar);
		ks.setBackground_partials(background_partials);
		//*/
		
		return thr;



	}
}
