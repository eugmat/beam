#!/usr/bin/python
#created by noise
from __future__ import print_function
import sys
import os
import warnings

try:
	import itertools
	import matplotlib as mpl
	mpl.use('Agg')
	import matplotlib.pyplot as plt
	import numpy as np
	import peakutils
	from scipy.stats import ks_2samp
except ImportError as e:
	print("[WARNING]Import error: {0}".format(e))	
	print("[WARNING]Please make sure to have the following python packages to access post analyses:\nitertools\nmatplotlib\nnumpy\npeakutils\nscipy\n\n")
	print("[WARNING]DO NOT WORRY, nothing is lost: you can run the python script separately on the results/<filename> folder to have it process everything")
	print("[INFO]Simply run 'python post_analysis.py results/<filename>'")
	quit()

warnings.filterwarnings("ignore")

"""Input: results folder path
"""

#MAIN pt.1
if len(sys.argv)<2:
	print("\nERROR: please specify results path\n")

respath=sys.argv[1]
motpath=os.path.join(respath,"benchmark/motifs/")


models=[]
for root, dirs, files in os.walk(motpath):
	for name in files: 
		if not name.endswith("gauss") and not name.startswith("cross") and (name.endswith(".search.nuc.txt") or name.endswith(".search.txt")):
			#print(os.path.join(root,name))
			models.append(os.path.join(root,name))


#FUNCTIONS
def getDistances(model1,model2):
	"""returns an array of distances between models in RNAs containing both"""
	dist=[]
	mod1={}
	mod2={}
	#modelX is a path
	with open(model1) as f:
		line=f.readline()
		line=f.readline()
		while(line and line!="\n"):
			ch=line.split()
			
			if not ch[1].split("$")[0].endswith("_bg"):
				name=ch[1]
				start=int(ch[3])
				mod1[name] = start
				#next: use set to intersect keys (IDs)
			line=f.readline()
	with open(model2) as f:
		line=f.readline()
		line=f.readline()
		while(line and line!="\n"):
			ch=line.split()
			
			if not ch[1].split("$")[0].endswith("_bg"):
				name=ch[1]
				start=int(ch[3])
				mod2[name] = start
				#next: use set to intersect keys (IDs)
			line=f.readline()

	mod1Set= set(mod1)
	mod2Set= set(mod2)
	for matching in mod1Set.intersection(mod2Set):
		#print(mod1[matching], mod2[matching], matching)
		dist.append(mod1[matching] - mod2[matching])
	return dist

def plotCross(cross):
	"""uses pyplot to grid-colour models pair data"""
	zvals=[]
	models=[]
	zvalsMI=[]
	modelsMI=[]

	with open(cross) as f:
		line=f.readline()
		while(line):
			if line.startswith("#Cooccurrence"):
				line=f.readline()
				models = line.strip().split() #[1n,1,2n,2...]
				for i in np.arange(len(models)):
					line=f.readline()
					tmp=[float(x) for x in line.strip().split()[1:]]
					zvals.append(tmp)
			elif line.startswith("#MI"):
				line=f.readline()
				modelsMI = line.strip().split() #[1n,1,2n,2...]
				for i in np.arange(len(models)):
					line=f.readline()
					tmp=[float(x) for x in line.strip().split()[1:]]
					zvalsMI.append(tmp)				
			else: line=f.readline()

		zvals = np.array(zvals)
		zvalsMI = np.array(zvalsMI)

		fig = plt.figure(2)

		cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
		                                           ['white','red'],
		                                           256)

		img2 = plt.imshow(zvals,interpolation='nearest',
		                    cmap = cmap2)

		plt.colorbar(img2,cmap=cmap2)
		plt.xticks(np.arange(len(models)),models, rotation=45)
		plt.yticks(np.arange(len(models)),models)
		plt.title("Models Cooccurrence")
		fig.tight_layout()
		print("writing "+ respath + "/cooccurrence.png")
		fig.savefig(respath+"/cooccurrence.png")
		plt.close()

		fig = plt.figure(2)

		cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
		                                           ['white','red'],
		                                           256)

		img2 = plt.imshow(zvalsMI,interpolation='nearest',
		                    cmap = cmap2)

		plt.colorbar(img2,cmap=cmap2)
		plt.xticks(np.arange(len(modelsMI)),modelsMI, rotation=45)
		plt.yticks(np.arange(len(modelsMI)),modelsMI)
		plt.title("Models Mutual Information")
		fig.tight_layout()
		print("writing "+ respath + "/MI.png")
		fig.savefig(respath+"/MI.png")
		plt.close()


def peakRecog(data):
	indexes=[]
	try:
		indexes = peakutils.indexes(data,thres=0.8, min_dist=3)
	except ValueError:
		#print("[INFO] no interesting pair single peaks found.")
		pass
	return indexes
# [ 333  693 1234 1600]

test = [1,1,1,1,1,20,1,1,1,1,1,1,1,1,1,1,1,5,14,7,2,3,4,1,2,3,4,1,2,3,1,7,0,15]
#print(test, len(test))
test2 = np.array([  1.,   1.,   0. ,  2. ,  0. ,  0.  , 1.  , 1.,   4. ,  6. ,  5. ,  6.,   3. ,  4.,  6., 10.,11.,
	10.  , 7. ,  3.  , 4.   ,6., 14.,   5. ,  5.,   5. , 12. , 17. , 10. , 12.,  20.,  14.,
  15. ,  7. , 16. , 20. , 26. , 22.,  29. , 32.,  25.,  37.,  26. , 20.,  40.,  53.,  24.,
  22. , 27. , 28. , 32. , 34. , 32.,  37. , 28. , 27.  ,33.,  23. , 25.,  32.,  28. , 30.,
  32. , 33., 27. , 24. , 17. , 28.,  17.,  15.,  19.,  20.,  20.,  12.,  12. , 13.,  10.,
   9.,  12. ,  7. ,  2. ,  5. ,  7.,   5. ,  1. ,  1. ,  2.  , 2.])
wbins=10 #bin width
interesting=[]

def plotScoresDistributions(file,motpath):
	#print(file)
	with open(motpath+file) as f:
		scores=[]
		scoresbg=[]
		secondSection=False
		line=f.readline()
		line=f.readline()

		while(line):
			if line =="\n" and not secondSection:
				line=f.readline()
				line=f.readline() #otherMatches	
				secondSection=True
			if line=="\n":
				break			
			if line.split()[1].split("$")[0].endswith("_bg"):
				scoresbg.append(float(line.split()[5]))
			else:
				scores.append(float(line.split()[5]))
			line=f.readline()

	binwidth = 1
	try:
		minmin=int(min(min(scores), min(scoresbg)))-1
		maxmax=int(max(max(scores),max(scoresbg)))+1
	except:
		try:
			minmin=min(scores)
			maxmax=max(scores)
		except:
			raise Exception("no sufficient data for {} to plot scores distributions".format(file)  )
	plt.hist(scores,normed=True, alpha=0.5, label="input", bins=np.arange(minmin, maxmax + binwidth, binwidth))
	try:
		plt.hist(scoresbg,normed=True, alpha=0.5, label="background", bins=np.arange(minmin, maxmax + binwidth, binwidth))
	except IndexError:
		pass
	plt.legend(loc='upper left')
	plt.xlabel("BEAM partial scores")
	plt.ylabel("relative frequency")

	m=file.split(".")[0].split("_")[-2]
	if file.split(".")[2] == "nuc":
		m += "_nuc"
	plt.title(m)

	pname=motpath + m + "_scores.png"
	plt.savefig(pname)
	plt.close()

def plotPositionDistribution(file,motpath):
	#print(file)
	with open(motpath+file) as f:
		position=[]
		positionbg=[]
		positionLow=[] #in the input but below threshold
		secondSection=False
		line=f.readline()
		line=f.readline()

		while(line):
			if line =="\n" and not secondSection:
				line=f.readline()
				line=f.readline() #otherMatches	
				secondSection=True
			if line=="\n":
				break			
			if line.split()[1].split("$")[0].endswith("_bg"):
				positionbg.append(int(line.split()[3]))
			else:
				if secondSection:
					positionLow.append(int(line.split()[3]))
				else:
					position.append(int(line.split()[3]))

			line=f.readline()

	binwidth = 10
	try:
		minmin=int(min(min(position), min(positionbg)))-1
		maxmax=int(max(max(position),max(positionbg)))+1
	except:
		try:
			minmin=min(position)
			maxmax=max(position)
		except:
			raise Exception("no sufficient data for {} to plot position distributions".format(file)  )
	try:
		plt.hist(positionbg,normed=True, alpha=0.5, label="background", bins=np.arange(minmin, maxmax + binwidth, binwidth))
	except IndexError:
		pass
	try:
		plt.hist(positionLow,normed=True, alpha=0.5, label="input_lowsc", bins=np.arange(minmin, maxmax + binwidth, binwidth))
	except IndexError:
		pass	
	plt.hist(position,normed=True, alpha=0.8, label="input", bins=np.arange(minmin, maxmax + binwidth, binwidth))

	plt.legend(loc='upper left')
	plt.xlabel("motif position")
	plt.ylabel("relative frequency")

	m=file.split(".")[0].split("_")[-2]
	if file.split(".")[2] == "nuc":
		m += "_nuc"
	plt.title(m)

	pname=motpath + m + "_pos.png"
	plt.savefig(pname)
	plt.close()

#MAIN pt.2


for pair in itertools.combinations(models,2):
	couple=pair[0].split("_m")[1].split("_")[0]
	if "nuc" in pair[0].split(".")[-2]:
		couple += "n"
	couple += pair[1].split("_m")[1].split("_")[0]
	if "nuc" in pair[1].split(".")[-2]:
		couple += "n"

	#data array
	data=getDistances(pair[0],pair[1])
	#####
	if len(data) > 0:
		#style
		plt.xlabel("motif start distance " + couple)
		counts,bins,bars=plt.hist(data, bins=range(min(data), max(data) + 1, wbins))
		#print(couple)
		peaks=peakRecog(np.array(counts))


#PLOT pair distances and pvalues
		plotName=respath+"/distance/"+ couple+".png"
		plt.savefig(plotName, bbox_inches='tight')

		plt.close()
		if len(peaks)==1:

			peakAt=int((bins[peaks]+bins[peaks+1])*0.5)
			m=min(data)
			M=max(data)
			mode=(M+m)*0.5
			nullhp=[np.random.triangular(m,mode,M) for i in range(10000)]
			nullCounts,_,_=plt.hist(nullhp, bins=range(m,M+1,wbins), normed=True)
			normedCounts,_,_=plt.hist(data, bins=range(min(data), max(data) + 1, wbins), normed=True)
			plt.close()
			D,p=ks_2samp(normedCounts,nullCounts)
			interesting.append([couple,D,p,peakAt])

#PLOT scores distributions
print("plotting scores distributions and position (full input against background)")
for file in os.listdir(motpath):
	if file.endswith(".search.txt") or file.endswith(".search.nuc.txt"):
		plotScoresDistributions(file, motpath)
		plotPositionDistribution(file, motpath)

#PLOT COoccurrence & Mutual Info
cross=motpath+"/cross_analysis.txt"
print("plotting cross analyses")

plotCross(cross)

with open(respath+"/distance_peaks.txt" ,'w') as o:
	interestingRow="#Model pairs with peaks in distance distributions:\npair\tKS\tp-value\tdistance"
	for i in range(len(interesting)):
		interestingRow += "\n" + "\t".join(map(str,interesting[i]))
	o.write(interestingRow)

print("run END\ninteresting pairs (single peaks in distance plots): " + ",".join([x[0] for x in interesting]))
